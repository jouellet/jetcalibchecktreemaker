#  Settings for Release 20.7 jet area + residual offset + absolute EtaJES correction + Global Sequential Calibration + In-situ
#
#  Config file author: Jonathan Bossio and Steve Alkire, Nov 2017 (last modification)
#
#####################

# ----------------
# 1. Absolute JES

  # The file with the absolute JES factors
AbsoluteJES.CalibFile:	       JetCalibTools/CalibrationFactors/MC15_JES_r7676_FINE_Aug2016.config
AbsoluteJES.Description:       JES derived Sep 2016 EM PFlow

JES.EtaBins: -4.5 -4.4 -4.3 -4.2 -4.1 -4.0
+JES.EtaBins: -3.9 -3.8 -3.7 -3.6 -3.5 -3.4 -3.3 -3.2 -3.1 -3.0
+JES.EtaBins: -2.9 -2.8 -2.7 -2.6 -2.5 -2.4 -2.3 -2.2 -2.1 -2.0
+JES.EtaBins: -1.9 -1.8 -1.7 -1.6 -1.5 -1.4 -1.3 -1.2 -1.1 -1.0
+JES.EtaBins: -0.9 -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2 -0.1
+JES.EtaBins: 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9
+JES.EtaBins: 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9
+JES.EtaBins: 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9
+JES.EtaBins: 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9
+JES.EtaBins: 4.0 4.1 4.2 4.3 4.4 4.5

  # How low in ET do we extrapolate along the calibration curve ?
AntiKt4EMPFlow.MinPtForETAJES:  10

  # Which method should be used to extend the calibration curve past the minimum ET ?
  # 0 = calibation curve is frozen at minimum ET
  # 1 = slope of calibration curve is frozen at minimum ET (recommended)
  # 2 = order 2 polynomial is used to extend the calibration curve
LowPtJESExtrapolationMethod:   0

AntiKt4EMPFlow.FreezeJEScorrectionatHighE: true

# ----------------
# 2. Pile-up correction

  # What offset correction to apply
#unused OffsetCorrection.Name:          OffsetMC12Nov2012

  # What residual jet-area offset correction to apply
#ResidualOffsetCorrection.Name:   ResidualOffset
ResidualOffsetCorrection.Name:   FINEVPF1r7676

  # These corrections should correspond to the
  # conditions of the absolute JES calibration
OffsetCorrection.DefaultMuRef:   21
OffsetCorrection.DefaultNPVRef:  15

  # additional config files to include
ResidualOffset.CalibFile: JetCalibTools/CalibrationFactors/MCBasedOffset_r7676_Aug2016.config


  # mu-scale factor used (only for MC)
MuScaleFactor: 	      1.09

  # should the beam-spot correction be applied (only for MC)
ApplyNPVBeamspotCorrection:	0

  # Set this to 1 to apply full 4-vector area correction
ApplyFullJetArea4MomentumCorrection:     0
# ----------------
# # 4. Global sequential calibration

TurnOffTrackCorrections:  true  # Track corrections turned off linearly between TurnOffStartingpT and TurnOffEndpT
TurnOffStartingpT:       1200
TurnOffEndpT:            2000
PTResponseRequirementOff: true # Response>1 requirement

GSCFactorsFile: JetCalibTools/CalibrationFactors/MC15_GSC_factors_PFlow_Sep2016_3corr.root
#Order of GSC correction is ChargedFraction->Tile0->EM3->PunchThrough, use the GSCDepth flag to control the last correction applied
#Acceptable values for the GSC Depth flag are: "ChargedFraction", "Tile0", "EM3", "PunchThrough", or "Full" (equivalent to "PunchThrough")
GSCDepth: PunchThrough 
PunchThroughEtaBins: 0.0 1.3 1.9 2.7

######################

  # Relative JES correction applied to data only to correct for features not captured by MC
InsituCalibrationFile:           JetCalibTools/InsituCalibration/InsituCalibration_Nov_2017_PFlow.root
InsituCalibrationDescription:    In-situ calibration for 2016 data only
  # eta-intercalibration from dijet events
RelativeInsituCalibrationHistogram:      JETALGO_EtaInterCalibration
  # absolute scale from Z+jet, gamma+jet and multijet balance
AbsoluteInsituCalibrationHistogram:      JETALGO_InsituCalib

#
# ######################

