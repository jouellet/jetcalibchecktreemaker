#
#  Residual and absolute EtaJES corrections for Mu-scan MC15
#
#  Config file author: Jonathan Bossio, November 2015
#
#####################

# ----------------
# 1. Absolute JES

  # The file with the absolute JES factors
AbsoluteJES.CalibFile:         JetCalibTools/CalibrationFactors/Muscan/MC15_JES_Muscan_r6993_March2016.config
AbsoluteJES.Description:       JES derived December 2015 with Jet Areas, for application to Mu-scan MC15

JES.EtaBins: -4.0 -3.6 -3.3 -3.0 -2.7 -2.4 -2.1 -1.8 -1.5 -1.2 -0.9 -0.6 -0.3
+JES.EtaBins: 0.0 0.3 0.6 0.9 1.2 1.5 1.8 2.1 2.4 2.7 3.0 3.3 3.6 4.0

  # How low in ET do we extrapolate along the calibration curve ?
AntiKt4EMTopo.MinPtForETAJES:  8
AntiKt4LCTopo.MinPtForETAJES:  8

  # Which method should be used to extend the calibration curve past the minimum ET ?
  # 0 = calibation curve is frozen at minimum ET
  # 1 = slope of calibration curve is frozen at minimum ET (recommended)
  # 2 = order 2 polynomial is used to extend the calibration curve
LowPtJESExtrapolationMethod:   1
LowPtJESExtrapolationMinimumResponse: 0.25

# ----------------
# 2. Pile-up correction

  # What offset correction to apply
OffsetCorrection.Name:          OffsetMC12Nov2012

  # What residual jet-area offset correction to apply
ResidualOffsetCorrection.Name:   ResidualOffsetMuscanMC15Nov2015

  # These corrections should correspond to the
  # conditions of the absolute JES calibration
OffsetCorrection.DefaultMuRef:   0
OffsetCorrection.DefaultNPVRef:  1

  # additional config files to include
ResidualOffset.CalibFile:	      JetCalibTools/CalibrationFactors/Muscan/MCBasedOffset_muscan_r6993_Nov2015.config

  # mu-scale factor used (only for MC)
MuScaleFactor: 	      1.09

  # should the beam-spot correction be applied (only for MC)
ApplyNPVBeamspotCorrection:	0

  # Set this to 1 to apply full 4-vector area correction
ApplyFullJetArea4MomentumCorrection:     0


