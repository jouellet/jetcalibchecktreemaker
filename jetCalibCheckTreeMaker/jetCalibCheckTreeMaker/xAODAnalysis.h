#ifndef jetCalibCheckTreeMaker__xAODAnalysis_H
#define jetCalibCheckTreeMaker__xAODAnalysis_H

// standard eventloop includes
#include <EventLoop/Algorithm.h>
#include "AsgTools/AnaToolHandle.h"

// root includes
#include <TTree.h>
#include <TH1.h>
#include <vector>
#include <string>

// trigger tools
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

// GRL tool
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// track selection tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// jet cleaning tool
#include "JetInterface/IJetSelector.h"

// E/gamma tools
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
 
// Muon tools
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"

// Jet calibration tool
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"

// MC truth classification
#include "MCTruthClassifier/MCTruthClassifier.h"

using namespace std;

///** Details of the hadronic endcap ('HEC') cuts **/
//const double hecEtaLo = 1.5;
//const double hecEtaHi = 3.2;
//const double hecPhiLo = TMath::Pi();
//const double hecPhiHi = 3*TMath::Pi()/2;
//const double hecR = 0.2; // additional cut to reduce reconstruction bias

class xAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  float m_trkPtCut;
  float m_jetPtCut;
  float m_electronPtCut;
  float m_muonPtCut;
  float m_photonPtCut;
  string outputName;
  bool m_isCollisions;
  bool m_isOverlayMC; // for new egamma calibration
  bool m_getTracks;

  // jet calibration configurations
  const string jes_config_dir = "JetCalibTools/JetCalibTools/CalibArea-00-04-79/CalibrationConfigs/";
  //const string m_Akt4EMTopo_ConfigFile = "JES_data2016_data2015_Recommendation_Dec2016.config"; // overcalibrates jets - use HI
  string m_Akt4EMTopo_ConfigFile;
  string m_Akt4HI_EM_EtaJES_ConfigFile;
  string m_Akt4HI_Constit_EtaJES_ConfigFile;
  const string m_Akt4HI_Insitu_ConfigFile = "JES_MC15c_HI_Aug2018.config";

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  xAODAnalysis ();

  xAODAnalysis (const string config_file_hi_em, const string config_file_hi_constit, const string config_file_emtopo, const bool isMC, const bool isOverlayMC, const bool getTracks);

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  uint8_t getSum ( const xAOD::TrackParticle& trk, xAOD::SummaryType sumType ); 
  bool passMinBias ( const xAOD::TrackParticle& trk, const float vtx_z );// const xAOD::Vertex* vtx );

  float InTwoPi (float phi);
  float DeltaPhi (float phi1, float phi2);
  float DeltaR (const float eta1, const float eta2, const float phi1, const float phi2);
  

  // this is needed to distribute the algorithm to the workers
  ClassDef(xAODAnalysis, 1);

private:

  TH1* m_event_hist; //!

  TTree* m_tree; //!

  // Tree branches 
  int m_b_runNumber; //!
  int m_b_eventNumber; //!
  unsigned int m_b_lumiBlock; //! 
  float m_b_actualInteractionsPerCrossing; //!
  float m_b_averageInteractionsPerCrossing; //!

  // global event info
  int m_b_nvert; //!
  vector<float> m_b_vert_x; //!
  vector<float> m_b_vert_y; //!
  vector<float> m_b_vert_z; //!
  vector<int> m_b_vert_ntrk; //!
  vector<int> m_b_vert_type; //!

  // fcal energy
  float m_b_fcalA_et; //!
  float m_b_fcalC_et; //!

  // tracking info (0th or primary vertex only)
  int m_b_ntrk; //!
  vector<bool> m_b_trk_quality_4; //!
  vector<float> m_b_trk_d0; //!
  vector<float> m_b_trk_z0; //!
  vector<float> m_b_trk_theta; //!
  vector<float> m_b_trk_charge; //!
  vector<float> m_b_trk_pt; //!
  vector<float> m_b_trk_eta; //!
  vector<float> m_b_trk_phi; //!

  // electron trigger names
  static const int electronTrigLength = 1; //!
  const string m_electron_trig_string[ electronTrigLength ] = {
    "HLT_e15_lhloose_nod0"
  };

  // muon trigger names
  static const int muonTrigLength = 1; //!
  const string m_muon_trig_string[ muonTrigLength ] = {
    "HLT_mu15"
  };

  // photon trigger names
  static const int photonTrigLength = 6; //!
  const string m_photon_trig_string[ photonTrigLength ] = {
    "HLT_g10_loose",
    "HLT_g15_loose",
    "HLT_g20_loose",
    "HLT_g25_loose",
    "HLT_g30_loose",
    "HLT_g35_loose"
  };

  //static const int jetTrigLength = 7; //!
  //const string m_jet_trig_string[ jetTrigLength ] = {
  //  "HLT_j30_ion_0eta490_L1TE10", // First the ion jets for period A
  //  "HLT_j35_ion_n320eta490_L1TE10",
  //  "HLT_j40_ion_L1J5",
  //  "HLT_j50_ion_L1J10",
  //  "HLT_j60_ion_L1J20",
  //  "HLT_j90_ion_L1J20",
  //  "HLT_j100_ion_L1J20"
  //};

  // electron triggers
  bool m_b_electron_trig_bool[ electronTrigLength ]; //!
  float m_b_electron_trig_prescale[ electronTrigLength ]; //!

  // muon triggers
  bool m_b_muon_trig_bool[ muonTrigLength ]; //!
  float m_b_muon_trig_prescale[ muonTrigLength ]; //!

  // photon triggers
  bool m_b_photon_trig_bool[ photonTrigLength ]; //!
  float m_b_photon_trig_prescale[ photonTrigLength ]; //!

  //// jet triggers
  //bool m_b_jet_trig_bool[ jetTrigLength ]; //!
  //float m_b_jet_trig_prescale[ jetTrigLength ]; //!

  // Jets passing cleaning cut
  int m_b_total_jet_n; //!
  int m_b_clean_jet_n; //!

  // jet info
  int m_b_akt4hi_jet_n; //!
  vector<vector<double>> m_b_akt4hi_sampling; //!
  int m_b_akt4emtopo_jet_n; //!
  vector<vector<double>> m_b_akt4emtopo_sampling; //!

  // initial jet 4-momentum - em scale
  vector<float> m_b_akt4hi_em_jet_pt; //!
  vector<float> m_b_akt4hi_em_jet_eta; //!
  vector<float> m_b_akt4hi_em_jet_phi; //!
  vector<float> m_b_akt4hi_em_jet_e; //!
  // initial jet 4-momentum - constit
  vector<float> m_b_akt4hi_constit_jet_pt; //!
  vector<float> m_b_akt4hi_constit_jet_eta; //!
  vector<float> m_b_akt4hi_constit_jet_phi; //!
  vector<float> m_b_akt4hi_constit_jet_e; //!
  // initial emtopo jet 4-momentum at the em scale
  vector<float> m_b_akt4emtopo_em_jet_pt; //!
  vector<float> m_b_akt4emtopo_em_jet_eta; //!
  vector<float> m_b_akt4emtopo_em_jet_phi; //!
  vector<float> m_b_akt4emtopo_em_jet_e; //!

  // jet 4-momentum after EtaJES calibration - em scale
  vector<float> m_b_akt4hi_em_etajes_jet_pt; //!
  vector<float> m_b_akt4hi_em_etajes_jet_eta; //!
  vector<float> m_b_akt4hi_em_etajes_jet_phi; //!
  vector<float> m_b_akt4hi_em_etajes_jet_e; //!
  // jet 4-momentum after EtaJES calibration - constit scale
  vector<float> m_b_akt4hi_constit_etajes_jet_pt; //!
  vector<float> m_b_akt4hi_constit_etajes_jet_eta; //!
  vector<float> m_b_akt4hi_constit_etajes_jet_phi; //!
  vector<float> m_b_akt4hi_constit_etajes_jet_e; //!

  // jet 4-momentum after EtaJES and 2015 pp cross calibration - em scale
  vector<float> m_b_akt4hi_em_xcalib_jet_pt; //!
  vector<float> m_b_akt4hi_em_xcalib_jet_eta; //!
  vector<float> m_b_akt4hi_em_xcalib_jet_phi; //!
  vector<float> m_b_akt4hi_em_xcalib_jet_e; //!
  // jet 4-momentum after EtaJES and 2015 pp cross calibration - constit scale
  vector<float> m_b_akt4hi_constit_xcalib_jet_pt; //!
  vector<float> m_b_akt4hi_constit_xcalib_jet_eta; //!
  vector<float> m_b_akt4hi_constit_xcalib_jet_phi; //!
  vector<float> m_b_akt4hi_constit_xcalib_jet_e; //!
  // emtopo jet 4-momentum after calibrations
  vector<float> m_b_akt4emtopo_calib_jet_pt; //!
  vector<float> m_b_akt4emtopo_calib_jet_eta; //!
  vector<float> m_b_akt4emtopo_calib_jet_phi; //!
  vector<float> m_b_akt4emtopo_calib_jet_e; //!

  // sum of track pTs
  vector<float> m_b_akt4hi_jet_SumPtTrkPt500; //!
  vector<float> m_b_akt4hi_jet_SumPtTrkPt1000; //!
  vector<float> m_b_akt4hi_jet_SumPtTrkPt1500; //!
  vector<float> m_b_akt4hi_jet_SumPtTrkPt2000; //!
  vector<float> m_b_akt4hi_jet_SumPtTrkPt2500; //!
  vector<float> m_b_akt4emtopo_jet_SumPtTrkPt500; //!
  vector<float> m_b_akt4emtopo_jet_SumPtTrkPt1000; //!
  vector<float> m_b_akt4emtopo_jet_SumPtTrkPt1500; //!
  vector<float> m_b_akt4emtopo_jet_SumPtTrkPt2000; //!
  vector<float> m_b_akt4emtopo_jet_SumPtTrkPt2500; //!

  // truth jet info
  int m_b_truth_jet_n; //!
  vector<float> m_b_truth_jet_pt; //!
  vector<float> m_b_truth_jet_phi; //!
  vector<float> m_b_truth_jet_eta; //!
  vector<float> m_b_truth_jet_e; //!

  // electron info
  int m_b_electron_n; //!
  vector<float> m_b_electron_pt; //!
  vector<float> m_b_electron_eta; //!
  vector<float> m_b_electron_phi; //!
  vector<int> m_b_electron_charge; //!
  
  // truth electron info
  int m_b_truth_electron_n; //!
  vector<float> m_b_truth_electron_pt; //!
  vector<float> m_b_truth_electron_eta; //!
  vector<float> m_b_truth_electron_phi; //!
  vector<int> m_b_truth_electron_charge; //!
  vector<unsigned int> m_b_truth_electron_type; //!
  vector<unsigned int> m_b_truth_electron_origin; //!

  // electron id
  vector<bool> m_b_electron_loose; //!
  vector<bool> m_b_electron_tight; //!
  vector<float> m_b_electron_d0sig; //!
  vector<float> m_b_electron_delta_z0_sin_theta; //!

  // muon info
  int m_b_muon_n; //!
  vector<float> m_b_muon_pt; //!
  vector<float> m_b_muon_eta; //!
  vector<float> m_b_muon_phi; //!
  vector<int> m_b_muon_charge; //!

  // truth muon info
  int m_b_truth_muon_n; //!
  vector<float> m_b_truth_muon_pt; //!
  vector<float> m_b_truth_muon_eta; //!
  vector<float> m_b_truth_muon_phi; //!
  vector<int> m_b_truth_muon_charge; //!
  vector<unsigned int> m_b_truth_muon_type; //!
  vector<unsigned int> m_b_truth_muon_origin; //!

  // muon id
  vector<int> m_b_muon_quality; //!
  vector<bool> m_b_muon_tight; //!
  vector<bool> m_b_muon_loose; //!

  // photon info
  int m_b_photon_n; //!
  vector<float> m_b_photon_pt; //!
  vector<float> m_b_photon_eta; //!
  vector<float> m_b_photon_phi; //!

  // truth photon info
  int m_b_truth_photon_n; //!
  vector<float> m_b_truth_photon_pt; //!
  vector<float> m_b_truth_photon_eta; //!
  vector<float> m_b_truth_photon_phi; //!
  vector<unsigned int> m_b_truth_photon_type; //!
  vector<unsigned int> m_b_truth_photon_origin; //!

  // photon id
  vector<bool> m_b_photon_tight; //!
  vector<bool> m_b_photon_loose; //!
  vector<unsigned int> m_b_photon_isem; //!
  vector<int> m_b_photon_convFlag; //!
  vector<float> m_b_photon_Rconv; //!
  vector<float> m_b_photon_topoetcone40; //!

  // GRL tool
  GoodRunsListSelectionTool* m_grl; //!

  // Trigger tools
  Trig::TrigDecisionTool* m_trigDecisionTool; //!
  TrigConf::xAODConfigTool* m_trigConfigTool; //!

  // Track selection tool
  InDet::InDetTrackSelectionTool* m_trackSelectionTool_4; //!

  // Jet tools
  asg::AnaToolHandle<IJetSelector> m_jetCleaningToolHandle; //!
  // EMTopo jets calibration
  JetCalibrationTool* m_Akt4EMTopo_CalibTool; //!
  // EtaJES calibration
  JetCalibrationTool* m_Akt4HI_Constit_EtaJES_CalibTool; //!
  JetCalibrationTool* m_Akt4HI_EM_EtaJES_CalibTool; //!
  // 2015 insitu cross calibration
  JetCalibrationTool* m_Akt4HI_Insitu_CalibTool; //!

  // Egamma tools
  AsgElectronLikelihoodTool* m_electronTightIsEMSelector; //!
  AsgElectronLikelihoodTool* m_electronLooseIsEMSelector; //!
  AsgPhotonIsEMSelector* m_photonTightIsEMSelector; //!
  AsgPhotonIsEMSelector* m_photonLooseIsEMSelector; //!
  CP::EgammaCalibrationAndSmearingTool* m_egammaPtEtaPhiECorrector; //!
  ElectronPhotonShowerShapeFudgeTool* m_fudgeMCTool; //!

  // Muon tools
  CP::MuonSelectionTool* m_muonTightSelector; //!
  CP::MuonSelectionTool* m_muonLooseSelector; //!
  CP::MuonCalibrationAndSmearingTool* m_muonPtEtaPhiECorrector; //!

  // MC truth
  //MCTruthClassifier* m_mcTruthClassifier; //!

};

#endif
