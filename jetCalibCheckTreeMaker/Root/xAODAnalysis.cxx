#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <jetCalibCheckTreeMaker/xAODAnalysis.h>

// root includes
#include <TFile.h>
#include <TSystem.h>
#include "xAODRootAccess/TStore.h"
#include "xAODCore/AuxContainerBase.h"

// xAOD includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

// lepton & photon xAOD includes
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/Muon.h>
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODTrigger/EmTauRoIContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthVertex.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
//#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

#include "PathResolver/PathResolver.h"

// ASG status code check
#include <AsgTools/MessageCheck.h>


// this is needed to distribute the algorithm to the workers
ClassImp(xAODAnalysis)


xAODAnalysis :: xAODAnalysis ():
  m_isCollisions(false),
  m_isOverlayMC(false),
  m_getTracks(false),
  m_trkPtCut(0.5),
  m_jetPtCut(5),
  m_electronPtCut(5),
  m_muonPtCut(5),
  m_photonPtCut(5),
  m_grl(NULL),
  m_trigDecisionTool(NULL),
  m_trigConfigTool(NULL),
  m_Akt4EMTopo_CalibTool(NULL),
  m_Akt4HI_Constit_EtaJES_CalibTool(NULL),
  m_Akt4HI_EM_EtaJES_CalibTool(NULL),
  m_Akt4HI_Insitu_CalibTool(NULL),
  m_electronTightIsEMSelector(NULL),
  m_electronLooseIsEMSelector(NULL),
  m_photonTightIsEMSelector(NULL),
  m_egammaPtEtaPhiECorrector(NULL),
  m_muonTightSelector(NULL),
  m_muonLooseSelector(NULL),
  m_muonPtEtaPhiECorrector(NULL)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
}



xAODAnalysis :: xAODAnalysis (const string config_file_hi_em, const string config_file_hi_constit, const string config_file_emtopo, const bool isMC, const bool isOverlayMC, const bool getTracks):
  m_trkPtCut(0.5),
  m_jetPtCut(5),
  m_electronPtCut(5),
  m_muonPtCut(5),
  m_photonPtCut(5),
  m_grl(NULL),
  m_trigDecisionTool(NULL),
  m_trigConfigTool(NULL),
  m_Akt4EMTopo_CalibTool(NULL),
  m_Akt4HI_Constit_EtaJES_CalibTool(NULL),
  m_Akt4HI_EM_EtaJES_CalibTool(NULL),
  m_Akt4HI_Insitu_CalibTool(NULL),
  m_electronTightIsEMSelector(NULL),
  m_electronLooseIsEMSelector(NULL),
  m_photonTightIsEMSelector(NULL),
  m_egammaPtEtaPhiECorrector(NULL),
  m_muonTightSelector(NULL),
  m_muonLooseSelector(NULL),
  m_muonPtEtaPhiECorrector(NULL) 
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  m_isCollisions = !isMC;
  m_Akt4HI_EM_EtaJES_ConfigFile = config_file_hi_em;
  m_Akt4HI_Constit_EtaJES_ConfigFile = config_file_hi_constit;
  m_Akt4EMTopo_ConfigFile = config_file_emtopo;
  m_isOverlayMC = isOverlayMC;
  m_getTracks = getTracks;
}



EL::StatusCode xAODAnalysis :: setupJob (EL::Job& job)
{
  job.useXAOD ();
  xAOD::Init(); // call before opening first file
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  TFile *outputFile = wk()->getOutputFile (outputName);

  m_event_hist = new TH1I("event_hist", "event_hist", 3, -0.5, 2.5);
  //wk()->addOutput (m_event_hist);
  m_event_hist->SetDirectory (outputFile);

  m_tree = new TTree ("tree", "tree");
  m_tree->SetDirectory (outputFile);

  ////////////////////////////////////////////////////////////////////////
  // Branch TTree
  ////////////////////////////////////////////////////////////////////////

  // collision info
  if (m_isCollisions) {
    m_tree->Branch ("runNumber", &m_b_runNumber, "runNumber/I");
    m_tree->Branch ("eventNumber", &m_b_eventNumber, "eventNumber/I");
    m_tree->Branch ("lumiBlock", &m_b_lumiBlock, "lumiBlock/i");
  }
  m_tree->Branch ("actualInteractionsPerCrossing", &m_b_actualInteractionsPerCrossing, "actualInteractionsPerCrossing/F");
  m_tree->Branch ("averageInteractionsPerCrossing", &m_b_averageInteractionsPerCrossing, "averageInteractionsPerCrossing/F");

  // vertex info
  m_tree->Branch ("nvert", &m_b_nvert, "nvert/I");
  m_tree->Branch ("vert_x", &m_b_vert_x);//,"vert_x[nvert]/F");
  m_tree->Branch ("vert_y", &m_b_vert_y);//,"vert_y[nvert]/F");
  m_tree->Branch ("vert_z", &m_b_vert_z);//,"vert_z[nvert]/F");
  m_tree->Branch ("vert_ntrk", &m_b_vert_ntrk);//,"vert_ntrk[nvert]/I");
  m_tree->Branch ("vert_type", &m_b_vert_type);//,"vert_type[nvert]/I");

  // FCAL sum Et info
  m_tree->Branch ("fcalA_et", &m_b_fcalA_et, "fcalA_et/F");
  m_tree->Branch ("fcalC_et", &m_b_fcalC_et, "fcalC_et/F");

  // tracking info
  if (m_getTracks) {
    m_tree->Branch ("ntrk", &m_b_ntrk, "ntrk/I");
    //m_tree->Branch ("sumpT", &m_b_sumpT, "sumpT/I");
    m_tree->Branch ("trk_quality_4", &m_b_trk_quality_4);
    m_tree->Branch ("trk_d0", &m_b_trk_d0);
    m_tree->Branch ("trk_z0", &m_b_trk_z0);
    m_tree->Branch ("trk_theta", &m_b_trk_theta);
    m_tree->Branch ("trk_charge", &m_b_trk_charge);
    m_tree->Branch ("trk_pt", &m_b_trk_pt);
    m_tree->Branch ("trk_eta", &m_b_trk_eta);
    m_tree->Branch ("trk_phi", &m_b_trk_phi);
  }

  if (m_isCollisions) {
    // Electron triggers
    for(int i = 0; i < electronTrigLength; i++) {
      m_tree->Branch (m_electron_trig_string[i].c_str(), &m_b_electron_trig_bool[i], Form ("%s/O", m_electron_trig_string[i].c_str()));
      m_tree->Branch (Form ("%s_prescale", m_electron_trig_string[i].c_str()), &m_b_electron_trig_prescale[i], Form ("%s_prescale/F", m_electron_trig_string[i].c_str()));
    }

    // Muon triggers
    for(int i = 0; i < muonTrigLength; i++) {
      m_tree->Branch (m_muon_trig_string[i].c_str(), &m_b_muon_trig_bool[i], Form ("%s/O", m_muon_trig_string[i].c_str()));
      m_tree->Branch (Form ("%s_prescale", m_muon_trig_string[i].c_str()), &m_b_muon_trig_prescale[i], Form ("%s_prescale/F", m_muon_trig_string[i].c_str()));
    }

    // Photon triggers
    for(int i = 0; i < photonTrigLength; i++) {
      m_tree->Branch (m_photon_trig_string[i].c_str(), &m_b_photon_trig_bool[i], Form ("%s/O", m_photon_trig_string[i].c_str()));
      m_tree->Branch (Form ("%s_prescale", m_photon_trig_string[i].c_str()), &m_b_photon_trig_prescale[i], Form ("%s_prescale/F", m_photon_trig_string[i].c_str()));
    }

    //// Jet triggers
    //for (int i = 0; i < jetTrigLength; i++) {
    //  m_tree->Branch (m_jet_trig_string[i].c_str(), &m_b_jet_trig_bool[i], Form ("%s/O", m_jet_trig_string[i].c_str()));
    //  m_tree->Branch (Form ("%s_prescale", m_jet_trig_string[i].c_str()), &m_b_jet_trig_prescale[i], Form ("%s_prescale/F", m_jet_trig_string[i].c_str()));
    //}
  }

  // Jets passing cleaning cut
  m_tree->Branch ("total_jet_n", &m_b_total_jet_n, "total_jet_n/I");
  m_tree->Branch ("clean_jet_n", &m_b_clean_jet_n, "clean_jet_n/I"); // clean_jet_n = total_jet_n - (number of dirty jets)
  
  // AntiKt4HIJets
  m_tree->Branch ("akt4hi_jet_n", &m_b_akt4hi_jet_n, "akt4hi_jet_n/I");
  //m_tree->Branch ("akt4hi_sampling", &m_b_akt4hi_sampling);

  m_tree->Branch ("akt4hi_em_jet_pt", &m_b_akt4hi_em_jet_pt);
  m_tree->Branch ("akt4hi_em_jet_eta", &m_b_akt4hi_em_jet_eta);
  m_tree->Branch ("akt4hi_em_jet_phi", &m_b_akt4hi_em_jet_phi);
  m_tree->Branch ("akt4hi_em_jet_e", &m_b_akt4hi_em_jet_e);

  m_tree->Branch ("akt4hi_em_etajes_jet_pt", &m_b_akt4hi_em_etajes_jet_pt);
  m_tree->Branch ("akt4hi_em_etajes_jet_eta", &m_b_akt4hi_em_etajes_jet_eta);
  m_tree->Branch ("akt4hi_em_etajes_jet_phi", &m_b_akt4hi_em_etajes_jet_phi);
  m_tree->Branch ("akt4hi_em_etajes_jet_e", &m_b_akt4hi_em_etajes_jet_e);

  m_tree->Branch ("akt4hi_em_xcalib_jet_pt", &m_b_akt4hi_em_xcalib_jet_pt);
  m_tree->Branch ("akt4hi_em_xcalib_jet_eta", &m_b_akt4hi_em_xcalib_jet_eta);
  m_tree->Branch ("akt4hi_em_xcalib_jet_phi", &m_b_akt4hi_em_xcalib_jet_phi);
  m_tree->Branch ("akt4hi_em_xcalib_jet_e", &m_b_akt4hi_em_xcalib_jet_e);

  m_tree->Branch ("akt4hi_constit_jet_pt", &m_b_akt4hi_constit_jet_pt);
  m_tree->Branch ("akt4hi_constit_jet_eta", &m_b_akt4hi_constit_jet_eta);
  m_tree->Branch ("akt4hi_constit_jet_phi", &m_b_akt4hi_constit_jet_phi);
  m_tree->Branch ("akt4hi_constit_jet_e", &m_b_akt4hi_constit_jet_e);

  m_tree->Branch ("akt4hi_constit_etajes_jet_pt", &m_b_akt4hi_constit_etajes_jet_pt);
  m_tree->Branch ("akt4hi_constit_etajes_jet_eta", &m_b_akt4hi_constit_etajes_jet_eta);
  m_tree->Branch ("akt4hi_constit_etajes_jet_phi", &m_b_akt4hi_constit_etajes_jet_phi);
  m_tree->Branch ("akt4hi_constit_etajes_jet_e", &m_b_akt4hi_constit_etajes_jet_e);

  m_tree->Branch ("akt4hi_constit_xcalib_jet_pt", &m_b_akt4hi_constit_xcalib_jet_pt);
  m_tree->Branch ("akt4hi_constit_xcalib_jet_eta", &m_b_akt4hi_constit_xcalib_jet_eta);
  m_tree->Branch ("akt4hi_constit_xcalib_jet_phi", &m_b_akt4hi_constit_xcalib_jet_phi);
  m_tree->Branch ("akt4hi_constit_xcalib_jet_e", &m_b_akt4hi_constit_xcalib_jet_e);

  m_tree->Branch ("akt4hi_jet_SumPtTrkPt500", &m_b_akt4hi_jet_SumPtTrkPt500);
  m_tree->Branch ("akt4hi_jet_SumPtTrkPt1000", &m_b_akt4hi_jet_SumPtTrkPt1000);
  m_tree->Branch ("akt4hi_jet_SumPtTrkPt1500", &m_b_akt4hi_jet_SumPtTrkPt1500);
  m_tree->Branch ("akt4hi_jet_SumPtTrkPt2000", &m_b_akt4hi_jet_SumPtTrkPt2000);
  m_tree->Branch ("akt4hi_jet_SumPtTrkPt2500", &m_b_akt4hi_jet_SumPtTrkPt2500);

  // AntiKt4EMTopoJets
  m_tree->Branch ("akt4emtopo_jet_n", &m_b_akt4emtopo_jet_n, "akt4emtopo_jet_n/I");
  //m_tree->Branch ("akt4emtopo_sampling", &m_b_akt4emtopo_sampling);

  m_tree->Branch ("akt4emtopo_em_jet_pt", &m_b_akt4emtopo_em_jet_pt);
  m_tree->Branch ("akt4emtopo_em_jet_eta", &m_b_akt4emtopo_em_jet_eta);
  m_tree->Branch ("akt4emtopo_em_jet_phi", &m_b_akt4emtopo_em_jet_phi);
  m_tree->Branch ("akt4emtopo_em_jet_e", &m_b_akt4emtopo_em_jet_e);

  m_tree->Branch ("akt4emtopo_calib_jet_pt", &m_b_akt4emtopo_calib_jet_pt);
  m_tree->Branch ("akt4emtopo_calib_jet_eta", &m_b_akt4emtopo_calib_jet_eta);
  m_tree->Branch ("akt4emtopo_calib_jet_phi", &m_b_akt4emtopo_calib_jet_phi);
  m_tree->Branch ("akt4emtopo_calib_jet_e", &m_b_akt4emtopo_calib_jet_e);

  m_tree->Branch ("akt4emtopo_jet_SumPtTrkPt500", &m_b_akt4emtopo_jet_SumPtTrkPt500);
  m_tree->Branch ("akt4emtopo_jet_SumPtTrkPt1000", &m_b_akt4emtopo_jet_SumPtTrkPt1000);
  m_tree->Branch ("akt4emtopo_jet_SumPtTrkPt1500", &m_b_akt4emtopo_jet_SumPtTrkPt1500);
  m_tree->Branch ("akt4emtopo_jet_SumPtTrkPt2000", &m_b_akt4emtopo_jet_SumPtTrkPt2000);
  m_tree->Branch ("akt4emtopo_jet_SumPtTrkPt2500", &m_b_akt4emtopo_jet_SumPtTrkPt2500);

  // Electrons
  m_tree->Branch ("electron_n", &m_b_electron_n, "electron_n/I");
  m_tree->Branch ("electron_pt", &m_b_electron_pt);
  m_tree->Branch ("electron_eta", &m_b_electron_eta);
  m_tree->Branch ("electron_phi", &m_b_electron_phi);
  m_tree->Branch ("electron_charge", &m_b_electron_charge);
  m_tree->Branch ("electron_tight", &m_b_electron_tight);
  m_tree->Branch ("electron_loose", &m_b_electron_loose);
  m_tree->Branch ("electron_d0sig", &m_b_electron_d0sig);
  m_tree->Branch ("electron_delta_z0_sin_theta", &m_b_electron_delta_z0_sin_theta);

  // Muons
  m_tree->Branch ("muon_n", &m_b_muon_n, "muon_n/I");
  m_tree->Branch ("muon_pt", &m_b_muon_pt);
  m_tree->Branch ("muon_eta", &m_b_muon_eta);
  m_tree->Branch ("muon_phi", &m_b_muon_phi);
  m_tree->Branch ("muon_quality", &m_b_muon_quality);
  m_tree->Branch ("muon_charge", &m_b_muon_charge);
  m_tree->Branch ("muon_tight", &m_b_muon_tight);
  m_tree->Branch ("muon_loose", &m_b_muon_loose);

  // Photons
  m_tree->Branch ("photon_n", &m_b_photon_n, "photon_n/I");
  m_tree->Branch ("photon_pt", &m_b_photon_pt);
  m_tree->Branch ("photon_eta", &m_b_photon_eta);
  m_tree->Branch ("photon_phi", &m_b_photon_phi);
  m_tree->Branch ("photon_tight", &m_b_photon_tight);
  m_tree->Branch ("photon_loose", &m_b_photon_loose);
  m_tree->Branch ("photon_isem", &m_b_photon_isem);
  m_tree->Branch ("photon_convFlag", &m_b_photon_convFlag);
  m_tree->Branch ("photon_Rconv", &m_b_photon_Rconv);
  m_tree->Branch ("photon_topoetcone40", &m_b_photon_topoetcone40);

  // MC truth information
  if (!m_isCollisions) {
    m_tree->Branch ("truth_jet_n", &m_b_truth_jet_n, "truth_jet_n/I");
    m_tree->Branch ("truth_jet_pt", &m_b_truth_jet_pt);
    m_tree->Branch ("truth_jet_eta", &m_b_truth_jet_eta);
    m_tree->Branch ("truth_jet_phi", &m_b_truth_jet_phi);
    m_tree->Branch ("truth_jet_e", &m_b_truth_jet_e);

    m_tree->Branch ("truth_electron_n", &m_b_truth_electron_n, "truth_electron_n/I");
    m_tree->Branch ("truth_electron_pt", &m_b_truth_electron_pt);
    m_tree->Branch ("truth_electron_eta", &m_b_truth_electron_eta);
    m_tree->Branch ("truth_electron_phi", &m_b_truth_electron_phi);
    m_tree->Branch ("truth_electron_charge", &m_b_truth_electron_charge);

    m_tree->Branch ("truth_muon_n", &m_b_truth_muon_n, "truth_muon_n/I");
    m_tree->Branch ("truth_muon_pt", &m_b_truth_muon_pt);
    m_tree->Branch ("truth_muon_eta", &m_b_truth_muon_eta);
    m_tree->Branch ("truth_muon_phi", &m_b_truth_muon_phi);
    m_tree->Branch ("truth_muon_charge", &m_b_truth_muon_charge);

    m_tree->Branch ("truth_photon_n", &m_b_truth_photon_n, "truth_photon_n/I");
    m_tree->Branch ("truth_photon_pt", &m_b_truth_photon_pt);
    m_tree->Branch ("truth_photon_eta", &m_b_truth_photon_eta);
    m_tree->Branch ("truth_photon_phi", &m_b_truth_photon_phi);
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  if (m_isCollisions) {

    // Initialize trigger configuration tool
    m_trigConfigTool = new TrigConf::xAODConfigTool ("xAODConfigTool"); // gives us access to the meta-data
    ANA_CHECK (m_trigConfigTool->initialize ());
    ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle (m_trigConfigTool);


    // Initialize trigger decision tool and connect to trigger configuration tool (NOTE: must be after trigger configuration tool is initialized!!!)
    m_trigDecisionTool = new Trig::TrigDecisionTool ("TrigDecisionTool");
    ANA_CHECK (m_trigDecisionTool->setProperty ("ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK (m_trigDecisionTool->setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool->initialize ());


    // Initialize GRL selection tool
    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    const char* GRLFilePath = "$ROOTCOREBIN/data/jetCalibCheckTreeMaker/data16_hip8TeV.periodAllYear_DetStatus-v86-pro20-19_DQDefects-00-02-04_PHYS_HeavyIonP_All_Good.xml";
    const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
    vector<string> vecStringGRL;
    vecStringGRL.push_back (fullGRLFilePath);
    ANA_CHECK (m_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_grl->initialize ());
  }


  // track selection tool
  m_trackSelectionTool_4 = new InDet::InDetTrackSelectionTool ("TrackSelectionTool");
  ANA_CHECK (m_trackSelectionTool_4->setProperty ("CutLevel", "TightPrimary"));
  //ANA_CHECK (m_trackSelectionTool_4->setProperty ("minPt", 0.500)); // cut on 500 MeV tracks
  ANA_CHECK (m_trackSelectionTool_4->setProperty ("maxZ0SinTheta", 1.0));
  ANA_CHECK (m_trackSelectionTool_4->setProperty ("maxNSiSharedHits", 100));
  ANA_CHECK (m_trackSelectionTool_4->initialize ());


  // Initialize jet cleaning tool
  m_jetCleaningToolHandle.setTypeAndName("JetCleaningTool/JetCleaning");
  ANA_CHECK(m_jetCleaningToolHandle.setProperty("CutLevel","LooseBad"));
  ANA_CHECK(m_jetCleaningToolHandle.setProperty("DoUgly", false));
  ANA_CHECK(m_jetCleaningToolHandle.retrieve ());


  // Initialize EMTopo jets JetCalibTool
  m_Akt4EMTopo_CalibTool = new JetCalibrationTool ("Akt4EMTopo_CalibTool");
  ANA_CHECK (m_Akt4EMTopo_CalibTool->setProperty ("JetCollection", "AntiKt4HI")); // pretend these are HI jets
  ANA_CHECK (m_Akt4EMTopo_CalibTool->setProperty ("ConfigFile", m_Akt4EMTopo_ConfigFile.c_str()));
  ANA_CHECK (m_Akt4EMTopo_CalibTool->setProperty ("DEVmode", true));
  //ANA_CHECK (m_Akt4EMTopo_CalibTool->setProperty ("ConfigDir", jes_config_dir.c_str()));
  ANA_CHECK (m_Akt4EMTopo_CalibTool->setProperty ("CalibSequence", "EtaJES_DEV"));
  ANA_CHECK (m_Akt4EMTopo_CalibTool->setProperty ("IsData", m_isCollisions));
  m_Akt4EMTopo_CalibTool->initializeTool ("Akt4EMTopo_CalibTool");


  // Initialize EtaJES JetCalibTool - EM scale calib
  m_Akt4HI_EM_EtaJES_CalibTool = new JetCalibrationTool ("Akt4HI_EM_EtaJES_CalibTool");
  ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("JetCollection", "AntiKt3HI"));
  ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("ConfigFile", m_Akt4HI_EM_EtaJES_ConfigFile.c_str()));
  ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("DEVmode", true));
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("OutputLevel", MSG::DEBUG)); // for debugging
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("ConfigDir", ""));
  ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("CalibSequence", "EtaJES_DEV")); // the calib sequence specifies that this tool should apply the EtaJES calibration
  ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("IsData", m_isCollisions));
  m_Akt4HI_EM_EtaJES_CalibTool->initializeTool ("Akt4HI_EM_EtaJES_CalibTool");


  // Initialize EtaJES JetCalibTool - constit scale calib
  m_Akt4HI_Constit_EtaJES_CalibTool = new JetCalibrationTool("Akt4HI_Constit_EtaJES_CalibTool");
  ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->setProperty ("JetCollection", "AntiKt4HI"));
  ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->setProperty ("ConfigFile", m_Akt4HI_Constit_EtaJES_ConfigFile.c_str()));
  ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->setProperty ("DEVmode", true));
  //ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->setProperty ("ConfigDir", ""));
  ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->setProperty ("CalibSequence", "EtaJES_DEV")); // the calib sequence specifies that this tool should apply the EtaJES calibration
  ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->setProperty ("IsData", m_isCollisions));
  m_Akt4HI_Constit_EtaJES_CalibTool->initializeTool("Akt4HI_Constit_EtaJES_CalibTool");


  // Initialize xCalib/Insitu JetCalibTool
  if (m_isCollisions) {
    m_Akt4HI_Insitu_CalibTool = new JetCalibrationTool ("Akt4HI_Insitu_CalibTool");
    ANA_CHECK (m_Akt4HI_Insitu_CalibTool->setProperty ("JetCollection", "AntiKt4HIJets"));
    ANA_CHECK (m_Akt4HI_Insitu_CalibTool->setProperty ("ConfigFile", m_Akt4HI_Insitu_ConfigFile.c_str()));
    ANA_CHECK (m_Akt4HI_Insitu_CalibTool->setProperty ("DEVmode", true));
    //ANA_CHECK (m_Akt4HI_Insitu_CalibTool->setProperty ("ConfigDir", jes_config_dir.c_str()));
    ANA_CHECK (m_Akt4HI_Insitu_CalibTool->setProperty ("CalibSequence", "Insitu_DEV")); // the calib sequence specifies that this tool should apply the insitu cross-calibrations
    ANA_CHECK (m_Akt4HI_Insitu_CalibTool->setProperty ("IsData", m_isCollisions));
    m_Akt4HI_Insitu_CalibTool->initializeTool ("Akt4HI_Insitu_CalibTool");
  }


  // Initialize egamma corrector tool - fixed overlay conditions version
  m_egammaPtEtaPhiECorrector = new CP::EgammaCalibrationAndSmearingTool ("EgammaPtEtaPhiECorrector");
  ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2015_5TeV"));
  ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("decorrelationModel", "FULL_v1")); // 1NP_v1
  ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("useAFII", 0));
  ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("isOverlayMC", m_isOverlayMC));
  //ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("OutputLevel", MSG::DEBUG));
  if (!m_isCollisions) ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("randomRunNumber", EgammaCalibPeriodRunNumbersExample::run_2016));
  ANA_CHECK(m_egammaPtEtaPhiECorrector->initialize ());

  //// Initialize egamma corrector tool - "recommended" version (without overlay fix)
  //m_egammaPtEtaPhiECorrector = new CP::EgammaCalibrationAndSmearingTool ("EgammaPtEtaPhiECorrector");
  //ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2016data_mc15c"));
  //ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("decorrelationModel", "FULL_v1")); // 1NP_v1
  //ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("useAFII", 0));
  ////ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("OutputLevel", MSG::DEBUG));
  //if (!m_isCollisions) ANA_CHECK(m_egammaPtEtaPhiECorrector->setProperty ("randomRunNumber", EgammaCalibPeriodRunNumbersExample::run_2016));
  //ANA_CHECK(m_egammaPtEtaPhiECorrector->initialize ());


  // Initialize electron tight selection tool
  m_electronTightIsEMSelector = new AsgElectronLikelihoodTool ("ElectronTightIsEMSelector");
  ANA_CHECK (m_electronTightIsEMSelector->setProperty ("WorkingPoint", "TightLHElectron"));
  ANA_CHECK (m_electronTightIsEMSelector->initialize ());


  // Initialize electron loose selection tool
  m_electronLooseIsEMSelector = new AsgElectronLikelihoodTool ("ElectronLooseIsEMSelector");
  ANA_CHECK (m_electronLooseIsEMSelector->setProperty ("WorkingPoint", "LooseLHElectron"));
  ANA_CHECK (m_electronLooseIsEMSelector->initialize ());


  // Initialize muon corrector tool
  m_muonPtEtaPhiECorrector = new CP::MuonCalibrationAndSmearingTool ("MuonPtEtaPhiECorrector");
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Year", "Data16"));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Release", "Recs2016_08_07"));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("StatComb", true));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorr", false));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaRelease","sagittaBiasDataAll_06_02_17"));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("doSagittaMCDistortion", false));
  ANA_CHECK (m_muonPtEtaPhiECorrector->initialize ());


  // Initialize muon loose selection tool
  m_muonLooseSelector = new CP::MuonSelectionTool ("MuonLooseSelection");
  ANA_CHECK (m_muonLooseSelector->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonLooseSelector->setProperty ("MuQuality", 2));
  ANA_CHECK (m_muonLooseSelector->initialize ());


  // Initialize muon tight selection tool
  m_muonTightSelector = new CP::MuonSelectionTool ("MuonTightSelection");
  ANA_CHECK (m_muonTightSelector->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonTightSelector->setProperty ("MuQuality", 0));
  ANA_CHECK (m_muonTightSelector->initialize ());


  // Initialize photon fudge factor tool
  if (!m_isCollisions) {
    m_fudgeMCTool = new ElectronPhotonShowerShapeFudgeTool ("FudgeMCTool");
    ANA_CHECK (m_fudgeMCTool->setProperty ("Preselection", 21)); // for release 20.x
    //ANA_CHECK (m_fudgeMCTool->setProperty ("FFCalibFile", "ElectronPhotonShowerShapeFudgeTool/v1/PhotonFudgeFactors.root"));
    ANA_CHECK (m_fudgeMCTool->initialize ());
  }


  // Initialize photon tight selection tool
  m_photonTightIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonTightIsEMSelector");
  ANA_CHECK (m_photonTightIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonTight));
  ANA_CHECK (m_photonTightIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf"));
  ANA_CHECK (m_photonTightIsEMSelector->initialize ());

  // Initialize photon loose selection tool
  m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonLooseIsEMSelector");
  ANA_CHECK (m_photonLooseIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonLoose));
  ANA_CHECK (m_photonLooseIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"));
  ANA_CHECK (m_photonLooseIsEMSelector->initialize ());


//  // Initialize MC truth tool
//  if (!m_isCollisions) {
//    m_mcTruthClassifier = new MCTruthClassifier ("MCTruthClassifier");
//    ANA_CHECK (m_mcTruthClassifier->initialize ());
//  }


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  event->retrieve (eventInfo, "EventInfo");

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  // check if the event is MC
  if (m_isCollisions && eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION))   {
    Error("execute()", "Tried to run non-MC specific code over MC. Exiting.");
    return EL::StatusCode::FAILURE;
  }   

  if (m_isCollisions) {
    m_b_runNumber = eventInfo->runNumber();
    m_b_eventNumber = eventInfo->eventNumber();
    m_b_lumiBlock = eventInfo->lumiBlock();
  }
  m_b_actualInteractionsPerCrossing = eventInfo->actualInteractionsPerCrossing();
  m_b_averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing();

  m_event_hist->Fill(0);

  if (m_isCollisions) {
    // if data check if event passes GRL
    if(!m_grl->passRunLB(*eventInfo)) return EL::StatusCode::SUCCESS; // go to next event
    m_event_hist->Fill(1); // events satisfies grl criterion

    // if events passes event cleaning
    if (eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ||
        eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ||
        eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ||
        eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)
       ) {
      return EL::StatusCode::SUCCESS; // 
    }
    m_event_hist->Fill(2); // event satisfies event cleaning criterion
  }

  ////////////////////////////////////////////////////////////////////////
  // Trigger information
  ////////////////////////////////////////////////////////////////////////
  if (m_isCollisions) {

    bool electronDidFire = false;
    bool muonDidFire = false;
    bool photonDidFire = false;
    //bool jetDidFire = false;

    // gather electron triggers
    for (int i = 0; i < electronTrigLength; i++) {
      auto t = m_trigDecisionTool->getChainGroup(m_electron_trig_string[i].c_str());
      m_b_electron_trig_bool[i] = t->isPassed();
      m_b_electron_trig_prescale[i] = t->getPrescale();

      // Make sure at least one trigger fired
      if(m_b_electron_trig_bool[i]) {
        electronDidFire = true;
      }
    }
    // gather muon triggers
    for (int i = 0; i < muonTrigLength; i++) {
      auto t = m_trigDecisionTool->getChainGroup(m_muon_trig_string[i].c_str());
      m_b_muon_trig_bool[i] = t->isPassed();
      m_b_muon_trig_prescale[i] = t->getPrescale();

      // Make sure at least one trigger fired
      if(m_b_muon_trig_bool[i]) {
        muonDidFire = true;
      }
    }
    // gather photon triggers
    for (int i = 0; i < photonTrigLength; i++) {
      auto t = m_trigDecisionTool->getChainGroup(m_photon_trig_string[i].c_str());
      m_b_photon_trig_bool[i] = t->isPassed();
      m_b_photon_trig_prescale[i] = t->getPrescale();

      // Make sure at least one trigger fired
      if (m_b_photon_trig_bool[i]) {
        photonDidFire = true;
      }
    }

    //// gather jet triggers
    //for (int i = 0; i < jetTrigLength; i++) {
    //  auto t = m_trigDecisionTool->getChainGroup(m_jet_trig_string[i].c_str());
    //  m_b_jet_trig_bool[i] = t->isPassed();
    //  m_b_jet_trig_prescale[i] = t->getPrescale();

    //  // Make sure at least one trigger fired
    //  if (m_b_jet_trig_bool[i]) {
    //    jetDidFire = true;
    //  }
    //}

    // select on events with an electron, muon, or photon trigger
    if (!muonDidFire && !electronDidFire && !photonDidFire) return EL::StatusCode::SUCCESS;

    //// alternatively select on inclusive jets
    //if (!jetDidFire) return EL::StatusCode::SUCCESS;

  }

  ////////////////////////////////////////////////////////////////////////
  // Clear vectorial data from previous event
  ////////////////////////////////////////////////////////////////////////
  {
    m_b_nvert = 0;
    m_b_vert_x.clear();
    m_b_vert_y.clear();
    m_b_vert_z.clear();
    m_b_vert_ntrk.clear();
    m_b_vert_type.clear();

    m_b_fcalA_et = 0;
    m_b_fcalC_et = 0;

    m_b_ntrk = 0;
    m_b_trk_quality_4.clear();
    m_b_trk_d0.clear();
    m_b_trk_z0.clear();
    m_b_trk_theta.clear();
    m_b_trk_charge.clear();
    m_b_trk_pt.clear();
    m_b_trk_eta.clear();
    m_b_trk_phi.clear();

    m_b_total_jet_n = 0;
    m_b_clean_jet_n = 0;
    m_b_akt4hi_jet_n = 0;
    m_b_akt4hi_sampling.clear();

    m_b_akt4hi_em_jet_pt.clear();
    m_b_akt4hi_em_jet_eta.clear();
    m_b_akt4hi_em_jet_phi.clear();
    m_b_akt4hi_em_jet_e.clear();

    m_b_akt4hi_em_etajes_jet_pt.clear();
    m_b_akt4hi_em_etajes_jet_eta.clear();
    m_b_akt4hi_em_etajes_jet_phi.clear();
    m_b_akt4hi_em_etajes_jet_e.clear();

    m_b_akt4hi_em_xcalib_jet_pt.clear();
    m_b_akt4hi_em_xcalib_jet_eta.clear();
    m_b_akt4hi_em_xcalib_jet_phi.clear();
    m_b_akt4hi_em_xcalib_jet_e.clear();

    m_b_akt4hi_constit_jet_pt.clear();
    m_b_akt4hi_constit_jet_eta.clear();
    m_b_akt4hi_constit_jet_phi.clear();
    m_b_akt4hi_constit_jet_e.clear();

    m_b_akt4hi_constit_etajes_jet_pt.clear();
    m_b_akt4hi_constit_etajes_jet_eta.clear();
    m_b_akt4hi_constit_etajes_jet_phi.clear();
    m_b_akt4hi_constit_etajes_jet_e.clear();

    m_b_akt4hi_constit_xcalib_jet_pt.clear();
    m_b_akt4hi_constit_xcalib_jet_eta.clear();
    m_b_akt4hi_constit_xcalib_jet_phi.clear();
    m_b_akt4hi_constit_xcalib_jet_e.clear();

    m_b_akt4hi_jet_SumPtTrkPt500.clear ();
    m_b_akt4hi_jet_SumPtTrkPt1000.clear ();
    m_b_akt4hi_jet_SumPtTrkPt1500.clear ();
    m_b_akt4hi_jet_SumPtTrkPt2000.clear ();
    m_b_akt4hi_jet_SumPtTrkPt2500.clear ();

    m_b_akt4emtopo_jet_n = 0;
    m_b_akt4emtopo_sampling.clear();

    m_b_akt4emtopo_em_jet_pt.clear();
    m_b_akt4emtopo_em_jet_eta.clear();
    m_b_akt4emtopo_em_jet_phi.clear();
    m_b_akt4emtopo_em_jet_e.clear();

    m_b_akt4emtopo_calib_jet_pt.clear();
    m_b_akt4emtopo_calib_jet_eta.clear();
    m_b_akt4emtopo_calib_jet_phi.clear();
    m_b_akt4emtopo_calib_jet_e.clear();

    m_b_akt4emtopo_jet_SumPtTrkPt500.clear ();
    m_b_akt4emtopo_jet_SumPtTrkPt1000.clear ();
    m_b_akt4emtopo_jet_SumPtTrkPt1500.clear ();
    m_b_akt4emtopo_jet_SumPtTrkPt2000.clear ();
    m_b_akt4emtopo_jet_SumPtTrkPt2500.clear ();

    m_b_electron_n = 0;
    m_b_electron_pt.clear();
    m_b_electron_eta.clear();
    m_b_electron_phi.clear();
    m_b_electron_charge.clear();
    m_b_electron_tight.clear();
    m_b_electron_loose.clear();
    m_b_electron_d0sig.clear();
    m_b_electron_delta_z0_sin_theta.clear();

    m_b_muon_n = 0;
    m_b_muon_pt.clear();
    m_b_muon_eta.clear();
    m_b_muon_phi.clear();
    m_b_muon_quality.clear();
    m_b_muon_charge.clear();
    m_b_muon_loose.clear();
    m_b_muon_tight.clear();

    m_b_photon_n = 0;
    m_b_photon_pt.clear();
    m_b_photon_eta.clear();
    m_b_photon_phi.clear();
    m_b_photon_tight.clear();
    m_b_photon_loose.clear();
    m_b_photon_isem.clear();
    m_b_photon_convFlag.clear();
    m_b_photon_Rconv.clear();
    m_b_photon_topoetcone40.clear();

    if (!m_isCollisions) {
      m_b_truth_jet_n = 0;
      m_b_truth_jet_pt.clear();
      m_b_truth_jet_phi.clear();
      m_b_truth_jet_eta.clear();
      m_b_truth_jet_e.clear();

      m_b_truth_electron_n = 0;
      m_b_truth_electron_pt.clear();
      m_b_truth_electron_eta.clear();
      m_b_truth_electron_phi.clear();
      m_b_truth_electron_charge.clear();

      m_b_truth_muon_n = 0;
      m_b_truth_muon_pt.clear();
      m_b_truth_muon_eta.clear();
      m_b_truth_muon_phi.clear();
      m_b_truth_muon_charge.clear();

      m_b_truth_photon_n = 0;
      m_b_truth_photon_pt.clear();
      m_b_truth_photon_eta.clear();
      m_b_truth_photon_phi.clear();
    }
  }


  ////////////////////////////////////////////////////////////////////////
  // Get vertex infos
  ////////////////////////////////////////////////////////////////////////
  float priVtxZ = 0;
  {
    const xAOD::VertexContainer* primaryVertices = 0;
    if  (!event->retrieve (primaryVertices, "PrimaryVertices") .isSuccess())  {
      Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    m_b_nvert = primaryVertices->size();
    /*xAOD::VertexContainer::const_iterator vertItr = primaryVertices->begin();
    xAOD::VertexContainer::const_iterator vertItrE = primaryVertices->end();
    int vert_counter = 0;
    for(; vertItr!=vertItrE; ++vertItr)
    {*/
    if (m_b_nvert == 0) return EL::StatusCode::SUCCESS; // select events with at least 1 vertex
    for (const auto* vert : *primaryVertices) {
      if (vert->vertexType() == xAOD::VxType::PriVtx) {
        priVtxZ = vert->z();
      }
      m_b_vert_x.push_back (vert->x());
      m_b_vert_y.push_back (vert->y());
      m_b_vert_z.push_back (vert->z());
      m_b_vert_ntrk.push_back (vert->nTrackParticles());
      m_b_vert_type.push_back (vert->vertexType());
      m_b_nvert++;
    }
  } // end vertex scope


  ////////////////////////////////////////////////////////////////////////
  // Calculated total FCal energies
  ////////////////////////////////////////////////////////////////////////
  {
    const xAOD::HIEventShapeContainer* hiueContainer = 0;  
    if (!event->retrieve (hiueContainer, "HIEventShape").isSuccess()) {
      Error ("execute()", "Failed to retrieve HIEventShape container. Exiting." );
      return EL::StatusCode::FAILURE; 
    }

    for (const auto* hiue : *hiueContainer) {
      double et = hiue->et() * 1e-3;
      int layer = hiue->layer();
      double eta = hiue->etaMin();

      if (layer == 21 || layer == 22 || layer == 23) {
        if (eta > 0) 
          m_b_fcalA_et += et;
        else 
          m_b_fcalC_et += et;
      }
    }
  } // end fcal scope


  ////////////////////////////////////////////////////////////////////////
  // Get tracking info
  ////////////////////////////////////////////////////////////////////////
  if (m_getTracks)
  {
    const xAOD::TrackParticleContainer* trackContainer = 0;
    if (!event->retrieve(trackContainer, "InDetTrackParticles").isSuccess()) {
      Error ("execute()", "Failed to retrieve InDetTrackParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
      
    for (const auto* track : *trackContainer) {
      //if (!passMinBias (*track, m_b_vert_z[0])) continue; // track selection
      if (track->pt() * 1e-3 < m_trkPtCut) continue;
      m_b_ntrk++;
      m_b_trk_pt.push_back (track->pt());
      m_b_trk_eta.push_back (track->eta());
      m_b_trk_phi.push_back (track->phi()); 
      m_b_trk_charge.push_back (track->charge());
      m_b_trk_quality_4.push_back (!m_trackSelectionTool_4->accept (*track));
      m_b_trk_d0.push_back (track->d0());
      m_b_trk_z0.push_back (track->z0());
      m_b_trk_theta.push_back (track->theta());
    } // end tracks loop
  } // end tracks scope


  ////////////////////////////////////////////////////////////////////////
  // Get AntiKt4EMTopo jets from container
  ////////////////////////////////////////////////////////////////////////
  {
    xAOD::TStore store;
    //xAOD::JetContainer* jets_calibrated = new xAOD::JetContainer();
    //xAOD::AuxContainerBase* jets_aux = new xAOD::AuxContainerBase();
    //jets_calibrated->setStore(jets_aux);
    //store->record(jets_calibrated, "jets_calibrated");
    //store->record(jets_aux, "jets_aux");

    const xAOD::JetContainer* jets = 0;
    if (!event->retrieve (jets, "AntiKt4EMTopoJets").isSuccess())  {
      Error("GetAntiKt4EMTopoJets()", "Failed to retrieve AntiKt4EMTopoJets collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    const xAOD::TrackParticleContainer* trackContainer = 0;
    if (!event->retrieve(trackContainer, "InDetTrackParticles").isSuccess()) {
      Error ("execute()", "Failed to retrieve InDetTrackParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* init_jet : *jets) {
      if (!m_jetCleaningToolHandle->keep (*init_jet))  continue; // implements jet cleaning by rejecting "dirty" jets

      xAOD::Jet* jet = new xAOD::Jet();
      jet->makePrivateStore(*init_jet);

      //vector<double> sampling = jet->getAttribute<vector<double>> ("EnergyPerSampling");

      // set pre-EtaJES 4-momentum at the EM and Constit scales
      xAOD::JetFourMom_t jet_4mom = jet->jetP4 ();
      const float em_pt = jet_4mom.pt();
      const float em_eta = jet_4mom.eta();
      const float em_phi = jet_4mom.phi();
      const float em_e = jet_4mom.e();

      //// HEC cut on uncalibrated jets + dR=0.2
      //// note that the HEC cut is required for both MC and data!
      //{
      //  const float phi_temp = (em_phi<0 ? em_phi+2*TMath::Pi() : em_phi);
      //  const float eta_temp= em_eta;
      //  if ((hecEtaLo-hecR < eta_temp && eta_temp < hecEtaHi+hecR) &&
      //      (hecPhiLo-hecR < phi_temp && phi_temp < hecPhiHi+hecR)) {
      //    if (jet) delete jet; // make sure to delete the copied jet!
      //    continue;
      //  }
      //}

      // apply calibration to shallow copy of jet
      jet->setJetP4("JetEMScaleMomentum", jet_4mom);
      ANA_CHECK (m_Akt4EMTopo_CalibTool->applyCalibration (*jet));

      // apply insitu correction to data
      if (m_isCollisions) {
        jet->setJetP4 ("JetGSCScaleMomentum", jet->jetP4());
        ANA_CHECK (m_Akt4HI_Insitu_CalibTool->applyCalibration (*jet));
      }

      jet_4mom = jet->jetP4();
      const float calib_pt = jet_4mom.pt();
      const float calib_eta = jet_4mom.eta();
      const float calib_phi = jet_4mom.phi();
      const float calib_e = jet_4mom.e();

      // pT cut on calibrated jets
      if (jet && calib_pt * 1e-3 < m_jetPtCut) {
        delete jet;
        continue;
      }

      float pttrk500 = 0., pttrk1000 = 0., pttrk1500 = 0., pttrk2000 = 0., pttrk2500 = 0.;
      for (const auto* track : *trackContainer) {
        //if (!passMinBias (*track, m_b_vert_z[0])) continue; // track selection
        if (DeltaR (calib_eta, track->eta (), calib_phi, track->phi ()) >= 0.4)
          continue;
        if (!m_trackSelectionTool_4->accept (*track))
          continue; // track selection
        const float pttrk = track->pt ();
        if (pttrk < 500)
          continue;
        pttrk500 += pttrk;
        if (pttrk < 1000)
          continue;
        pttrk1000 += pttrk;
        if (pttrk < 1500)
          continue;
        pttrk1500 += pttrk;
        if (pttrk < 2000)
          continue;
        pttrk2000 += pttrk;
        if (pttrk < 2500)
          continue;
        pttrk2500 += pttrk;
      }

      // Now write out all jet 4-momentum info
      m_b_akt4emtopo_em_jet_pt.push_back (em_pt * 1e-3);
      m_b_akt4emtopo_em_jet_eta.push_back (em_eta);
      m_b_akt4emtopo_em_jet_phi.push_back (em_phi);
      m_b_akt4emtopo_em_jet_e.push_back (em_e * 1e-3);

      m_b_akt4emtopo_calib_jet_pt.push_back (calib_pt * 1e-3);
      m_b_akt4emtopo_calib_jet_eta.push_back (calib_eta);
      m_b_akt4emtopo_calib_jet_phi.push_back (calib_phi);
      m_b_akt4emtopo_calib_jet_e.push_back (calib_e * 1e-3);

      m_b_akt4emtopo_jet_SumPtTrkPt500.push_back (pttrk500 * 1e-3);
      m_b_akt4emtopo_jet_SumPtTrkPt1000.push_back (pttrk1000 * 1e-3);
      m_b_akt4emtopo_jet_SumPtTrkPt1500.push_back (pttrk1500 * 1e-3);
      m_b_akt4emtopo_jet_SumPtTrkPt2000.push_back (pttrk2000 * 1e-3);
      m_b_akt4emtopo_jet_SumPtTrkPt2500.push_back (pttrk2500 * 1e-3);
      
      if (jet) delete jet;
      m_b_akt4emtopo_jet_n++;
      //m_b_akt4emtopo_sampling.push_back (sampling);
    } // end jet loop
    store.clear();
    //if (store) delete store;
  } // end jet scope


  ////////////////////////////////////////////////////////////////////////
  // Get AntiKt4HI jets from container
  ////////////////////////////////////////////////////////////////////////
  {
    // Use a TStore to avoid memory leaks- see documentation on cross-calibration for details
    xAOD::TStore store;
    //xAOD::JetContainer* jets_calibrated = new xAOD::JetContainer();
    //xAOD::AuxContainerBase* jets_aux = new xAOD::AuxContainerBase();
    //jets_calibrated->setStore(jets_aux);
    //store->record(jets_calibrated, "jets_calibrated");
    //store->record(jets_aux, "jets_aux");

    const xAOD::JetContainer* jets = 0;
    if (!event->retrieve (jets, "AntiKt4HIJets").isSuccess())  {
      Error("GetAntiKt4HIJets()", "Failed to retrieve AntiKt4HIJets collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    const xAOD::TrackParticleContainer* trackContainer = 0;
    if (!event->retrieve(trackContainer, "InDetTrackParticles").isSuccess()) {
      Error ("execute()", "Failed to retrieve InDetTrackParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* init_jet : *jets) {
      m_b_total_jet_n++;
      if (!m_jetCleaningToolHandle->keep (*init_jet))  continue; // implements jet cleaning by rejecting "dirty" jets
      m_b_clean_jet_n++;

      xAOD::Jet* jet = new xAOD::Jet();
      jet->makePrivateStore(*init_jet);

      //vector<double> sampling = jet->getAttribute<vector<double>> ("EnergyPerSampling");

      // set pre-EtaJES 4-momentum at the EM and Constit scales
      const xAOD::JetFourMom_t jet_4mom_em = jet->jetP4 ("JetEMScaleMomentum");
      const float em_pt = jet_4mom_em.pt();
      const float em_eta = jet_4mom_em.eta();
      const float em_phi = jet_4mom_em.phi();
      const float em_e = jet_4mom_em.e();

      const xAOD::JetFourMom_t jet_4mom_constit = jet->jetP4 ("JetConstitScaleMomentum");
      const float constit_pt = jet_4mom_constit.pt();
      const float constit_eta = jet_4mom_constit.eta();
      const float constit_phi = jet_4mom_constit.phi();
      const float constit_e = jet_4mom_constit.e();

      //// HEC cut on uncalibrated jets + dR=0.2
      //// note that the HEC cut is required for both MC and data!
      //{
      //  const float phi_temp = (em_phi<0 ? em_phi+2*TMath::Pi() : em_phi);
      //  const float eta_temp= em_eta;
      //  if ((hecEtaLo-hecR < eta_temp && eta_temp < hecEtaHi+hecR) &&
      //      (hecPhiLo-hecR < phi_temp && phi_temp < hecPhiHi+hecR)) {
      //    if (jet) delete jet; // make sure to delete the copied jet!
      //    continue;
      //  }
      //}

      // apply EtaJES calibration to the pileup scale
      jet->setJetP4 ("JetPileupScaleMomentum", jet_4mom_em);
      ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->applyCalibration (*jet));

      // set pre-insitu correction 4-momentum
      xAOD::JetFourMom_t jet_4mom = jet->jetP4();
      const float em_etajes_pt = jet_4mom.pt();
      const float em_etajes_eta = jet_4mom.eta();
      const float em_etajes_phi = jet_4mom.phi();
      const float em_etajes_e = jet_4mom.e();

      // etajes can only be reliably evaluated for truth pT (corrected pT) > 20 GeV
      //if(jet && em_etajes_pt * 1e-3 < m_jetPtCut) {
      //  delete jet;
      //  continue;
      //}

      // apply insitu correction to data
      if (m_isCollisions) {
        //cout << "Pre-insitu:  pt = " << jet->jetP4().pt() << ",\teta = " << jet->getAttribute<float>("DetectorEta") << endl;

        jet->setJetP4 ("JetGSCScaleMomentum", jet_4mom);
        ANA_CHECK (m_Akt4HI_Insitu_CalibTool->applyCalibration (*jet));

        //cout << "Post-insitu: pt = " << jet->jetP4().pt() << ",\teta = " << jet->getAttribute<float>("DetectorEta") << endl;
      }

      jet_4mom = jet->jetP4();
      const float em_xcalib_pt = jet_4mom.pt();
      const float em_xcalib_eta = jet_4mom.eta();
      const float em_xcalib_phi = jet_4mom.phi();
      const float em_xcalib_e = jet_4mom.e();

      // pT cut on calibrated jets
      if (jet && em_xcalib_pt * 1e-3 < m_jetPtCut) {
        delete jet;
        continue;
      }

      float pttrk500 = 0., pttrk1000 = 0., pttrk1500 = 0., pttrk2000 = 0., pttrk2500 = 0.;
      for (const auto* track : *trackContainer) {
        if (DeltaR (em_xcalib_eta, track->eta (), em_xcalib_phi, track->phi ()) >= 0.4)
          continue;
        if (!m_trackSelectionTool_4->accept (*track))
          continue; // track selection
        const float pttrk = track->pt ();
        if (pttrk < 500)
          continue;
        pttrk500 += pttrk;
        if (pttrk < 1000)
          continue;
        pttrk1000 += pttrk;
        if (pttrk < 1500)
          continue;
        pttrk1500 += pttrk;
        if (pttrk < 2000)
          continue;
        pttrk2000 += pttrk;
        if (pttrk < 2500)
          continue;
        pttrk2500 += pttrk;
      }

      // Now apply constit scale etajes corrections instead
      jet->setJetP4("JetPileupScaleMomentum", jet_4mom_constit);
      ANA_CHECK (m_Akt4HI_Constit_EtaJES_CalibTool->applyCalibration (*jet));

      // set pre-insitu correction 4-momentum
      jet_4mom = jet->jetP4();
      const float constit_etajes_pt = jet_4mom.pt();
      const float constit_etajes_eta = jet_4mom.eta();
      const float constit_etajes_phi = jet_4mom.phi();
      const float constit_etajes_e = jet_4mom.e();

      // etajes can only be reliably evaluated for truth pT (corrected pT) > 20 GeV
      //if(jet && constit_etajes_pt * 1e-3 < m_jetPtCut) {
      //  delete jet;
      //  continue;
      //}

      // apply insitu correction to data
      if (m_isCollisions) {
        jet->setJetP4 ("JetGSCScaleMomentum", jet_4mom);
        ANA_CHECK (m_Akt4HI_Insitu_CalibTool->applyCalibration (*jet));
      }

      jet_4mom = jet->jetP4();
      const float constit_xcalib_pt = jet_4mom.pt();
      const float constit_xcalib_eta = jet_4mom.eta();
      const float constit_xcalib_phi = jet_4mom.phi();
      const float constit_xcalib_e = jet_4mom.e();

      // Now write out all jet 4-momentum info
      m_b_akt4hi_em_jet_pt.push_back (em_pt * 1e-3);
      m_b_akt4hi_em_jet_eta.push_back (em_eta);
      m_b_akt4hi_em_jet_phi.push_back (em_phi);
      m_b_akt4hi_em_jet_e.push_back (em_e * 1e-3);

      m_b_akt4hi_em_etajes_jet_pt.push_back (em_etajes_pt * 1e-3);
      m_b_akt4hi_em_etajes_jet_eta.push_back (em_etajes_eta);
      m_b_akt4hi_em_etajes_jet_phi.push_back (em_etajes_phi);
      m_b_akt4hi_em_etajes_jet_e.push_back (em_etajes_e * 1e-3);

      m_b_akt4hi_em_xcalib_jet_pt.push_back (em_xcalib_pt * 1e-3);
      m_b_akt4hi_em_xcalib_jet_eta.push_back (em_xcalib_eta);
      m_b_akt4hi_em_xcalib_jet_phi.push_back (em_xcalib_phi);
      m_b_akt4hi_em_xcalib_jet_e.push_back (em_xcalib_e * 1e-3);

      m_b_akt4hi_constit_jet_pt.push_back (constit_pt * 1e-3);
      m_b_akt4hi_constit_jet_eta.push_back (constit_eta);
      m_b_akt4hi_constit_jet_phi.push_back (constit_phi);
      m_b_akt4hi_constit_jet_e.push_back (constit_e * 1e-3);

      m_b_akt4hi_constit_etajes_jet_pt.push_back (constit_etajes_pt * 1e-3);
      m_b_akt4hi_constit_etajes_jet_eta.push_back (constit_etajes_eta);
      m_b_akt4hi_constit_etajes_jet_phi.push_back (constit_etajes_phi);
      m_b_akt4hi_constit_etajes_jet_e.push_back (constit_etajes_e * 1e-3);

      m_b_akt4hi_constit_xcalib_jet_pt.push_back (constit_xcalib_pt * 1e-3);
      m_b_akt4hi_constit_xcalib_jet_eta.push_back (constit_xcalib_eta);
      m_b_akt4hi_constit_xcalib_jet_phi.push_back (constit_xcalib_phi);
      m_b_akt4hi_constit_xcalib_jet_e.push_back (constit_xcalib_e * 1e-3);

      m_b_akt4hi_jet_SumPtTrkPt500.push_back (pttrk500 * 1e-3);
      m_b_akt4hi_jet_SumPtTrkPt1000.push_back (pttrk1000 * 1e-3);
      m_b_akt4hi_jet_SumPtTrkPt1500.push_back (pttrk1500 * 1e-3);
      m_b_akt4hi_jet_SumPtTrkPt2000.push_back (pttrk2000 * 1e-3);
      m_b_akt4hi_jet_SumPtTrkPt2500.push_back (pttrk2500 * 1e-3);

      if (jet) delete jet;
      m_b_akt4hi_jet_n++;
      //m_b_akt4hi_sampling.push_back (sampling);
    } // end jet loop
    store.clear();
    //if (store) delete store;
  } // end jet scope

  if (!m_isCollisions && m_b_akt4hi_jet_n == 0) return EL::StatusCode::SUCCESS; // reject on 0 jets in data


  ////////////////////////////////////////////////////////////////////////
  // Get truth jets if MC
  ////////////////////////////////////////////////////////////////////////
  if (!m_isCollisions) {
    const xAOD::JetContainer* truthJetContainer = 0;
    if (!event->retrieve(truthJetContainer, "AntiKt4TruthJets").isSuccess()) {
      Error("GetAntiKt4TruthJets()", "Failed to retrieve AntiKt4TruthJets collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* truthJet : *truthJetContainer) {
      if (truthJet->pt()*1e-3 < m_jetPtCut)
        continue;
      m_b_truth_jet_pt.push_back (truthJet->pt()*1e-3);
      m_b_truth_jet_eta.push_back (truthJet->eta());
      m_b_truth_jet_phi.push_back (truthJet->phi());
      m_b_truth_jet_e.push_back (truthJet->e()*1e-3);
      m_b_truth_jet_n++;
    } // end truth jet loop
  } // end truth jet scope


  ////////////////////////////////////////////////////////////////////////
  // Get truth particles if MC
  ////////////////////////////////////////////////////////////////////////
  if (!m_isCollisions) {

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!event->retrieve (truthParticleContainer, "TruthParticles").isSuccess()) {
      Error ("TruthParticles()", "Failed to retrieve TruthParticles collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    for (const auto* truthParticle : *truthParticleContainer) {
    
      if (truthParticle->status() != 1) // if not final state continue
        continue;
      const int pdgId = truthParticle->pdgId();
      if (pdgId == 11 || pdgId == -11 ) { // if electron or anti-electron
        if (truthParticle->pt() * 1e-3 < m_electronPtCut) continue;
        m_b_truth_electron_pt.push_back (truthParticle->pt() * 1e-3);
        m_b_truth_electron_eta.push_back (truthParticle->eta());
        m_b_truth_electron_phi.push_back (truthParticle->phi());
        m_b_truth_electron_charge.push_back (truthParticle->charge());
        m_b_truth_electron_n++;
      }
      else if (pdgId == 22) { // if photon
        if (truthParticle->pt() * 1e-3 < m_photonPtCut) continue;
        m_b_truth_photon_pt.push_back (truthParticle->pt() * 1e-3);
        m_b_truth_photon_phi.push_back (truthParticle->phi());
        m_b_truth_photon_eta.push_back (truthParticle->eta());
        m_b_truth_photon_n++;
      }
      else if (pdgId == 13 || pdgId == -13) { // if muon or anti-muon
        if (truthParticle->pt() * 1e-3 < m_muonPtCut) continue;
        m_b_truth_muon_pt.push_back (truthParticle->pt() * 1e-3);
        m_b_truth_muon_phi.push_back (truthParticle->phi());
        m_b_truth_muon_eta.push_back (truthParticle->eta());
        m_b_truth_muon_charge.push_back (truthParticle->charge());
        m_b_truth_muon_n++;
      }
    } // end truth particles loop
  } // end truth particles scope


  ////////////////////////////////////////////////////////////////////////
  // Get electrons
  ////////////////////////////////////////////////////////////////////////
  {
    const xAOD::ElectronContainer* electrons = 0;
    if (!event->retrieve (electrons, "Electrons").isSuccess())  {
      Error("GetElectrons()", "Failed to retrieve Electrons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* init_electron : *electrons) {
      xAOD::Electron* electron = NULL;

      //const float initpt = init_electron->pt();

      if (m_egammaPtEtaPhiECorrector->correctedCopy(*init_electron, electron) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO("cannot calibrate electron!");
        continue;
      }
      //cout << "Init pT: " << initpt << ", final pT: " << electron->pt () * 1e-3 << ", % change: " << electron->pt () * 1e-3 * 100 / initpt - 100 << endl;

      if (electron && electron->pt() * 1e-3 < m_electronPtCut) {
        if (electron) delete electron;
        continue;
      }

      m_b_electron_pt.push_back (electron->pt() * 1e-3);
      m_b_electron_eta.push_back (electron->caloCluster()->etaBE(2));
      m_b_electron_phi.push_back (electron->phi());
      m_b_electron_charge.push_back (electron->charge());

      const xAOD::TrackParticle *tp = electron->trackParticle();
      m_b_electron_d0sig.push_back (xAOD::TrackingHelpers::d0significance (tp, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY()));
      m_b_electron_delta_z0_sin_theta.push_back (fabs(tp->z0() + tp->vz() - priVtxZ) * sin(tp->theta()));

      m_b_electron_tight.push_back (m_electronTightIsEMSelector->accept (electron));
      m_b_electron_loose.push_back (m_electronLooseIsEMSelector->accept (electron));

      if (electron) delete electron;
      m_b_electron_n++;
    } // end electron loop
  } // end electron scope


  ////////////////////////////////////////////////////////////////////////
  // Get muons
  ////////////////////////////////////////////////////////////////////////
  {
    const xAOD::MuonContainer* muons = 0;
    if (!event->retrieve (muons, "Muons").isSuccess())  {
      Error("GetMuons()", "Failed to retrieve Muons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* init_muon : *muons) {
      xAOD::Muon* muon = NULL;
      if (m_muonPtEtaPhiECorrector->correctedCopy(*init_muon, muon) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO("cannot calibrate muon!");
        continue;
      }

      if (muon->pt() * 1e-3 < m_muonPtCut) {
        if (muon) delete muon;
        continue;
      }

      m_b_muon_pt.push_back (muon->pt() * 1e-3);
      m_b_muon_eta.push_back (muon->eta());
      m_b_muon_phi.push_back (muon->phi());
      m_b_muon_quality.push_back (muon->quality());
      m_b_muon_charge.push_back (muon->charge());

      m_b_muon_loose.push_back (m_muonLooseSelector->accept (muon));
      m_b_muon_tight.push_back (m_muonTightSelector->accept (muon));

      if (muon) delete muon;
      m_b_muon_n++;
    } // end muon loop
  } // end muon scope


  ////////////////////////////////////////////////////////////////////////
  // Get photons
  ////////////////////////////////////////////////////////////////////////
  {
    const xAOD::PhotonContainer* photons = 0;
    if (!event->retrieve (photons, "Photons").isSuccess())  {
      Error("GetPhotons()", "Failed to retrieve Photons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* init_photon : *photons) {
      xAOD::Photon* photon = NULL;

      //const float initpt = init_photon->pt() * 1e-3;

      // Calibrate photon
      if (m_egammaPtEtaPhiECorrector->correctedCopy(*init_photon, photon) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO("cannot calibrate photon!");
        continue;
      }
      //cout << "Init pT: " << initpt << ", final pT: " << photon->pt () * 1e-3 << ", % change: " << photon->pt () * 1e-3 * 100 / initpt - 100 << endl;

      // Kinematic cut
      if (photon->pt() * 1e-3 < m_photonPtCut) {
        if (photon) delete photon;
        continue;
      }

      // Store kinematics
      m_b_photon_pt.push_back (photon->pt() * 1e-3);
      //m_b_photon_eta.push_back (photon->eta());
      m_b_photon_eta.push_back (photon->caloCluster()->etaBE(2));
      m_b_photon_phi.push_back (photon->phi());

      // If MC, get truth information and apply fudge factors to shower
      if (!m_isCollisions) {
        if (m_fudgeMCTool->applyCorrection(*photon) != CP::CorrectionCode::Ok) {
          ANA_MSG_INFO("cannot apply photon fudge factors!");
          continue;
        }
      }

      // Identification
      m_b_photon_tight.push_back (m_photonTightIsEMSelector->accept (photon));
      m_b_photon_loose.push_back (m_photonLooseIsEMSelector->accept (photon));
      m_b_photon_isem.push_back (m_photonTightIsEMSelector->IsemValue());
      m_b_photon_convFlag.push_back (xAOD::EgammaHelpers::conversionType (photon));
      m_b_photon_Rconv.push_back (xAOD::EgammaHelpers::conversionRadius (photon));
      m_b_photon_topoetcone40.push_back (photon->auxdata<float>("topoetcone40") * 1e-3);

      if (photon) delete photon;
      m_b_photon_n++;
    } // end photon loop
  } // end photon scope

  //if (m_b_photon_n < 1 && m_b_electron_n < 2 && m_b_muon_n < 2) return EL::StatusCode::SUCCESS; // reject on non- Z/gamma events

  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  // Delete trigger tools
  if (m_isCollisions && m_trigConfigTool)  {
    delete m_trigConfigTool;
    m_trigConfigTool = 0;
  }

  if (m_isCollisions && m_trigDecisionTool)  {
    delete m_trigDecisionTool;
    m_trigDecisionTool = 0;                     
  }

  // Delete GRL tool
  if (m_isCollisions && m_grl) {
    delete m_grl;
    m_grl = 0;
  }

  // Delete track selection tool
  if (m_trackSelectionTool_4) {
    delete m_trackSelectionTool_4;
    m_trackSelectionTool_4 = 0;
  }

  // Delete jet calibration tools
  if (m_Akt4EMTopo_CalibTool) {
    delete m_Akt4EMTopo_CalibTool;
    m_Akt4EMTopo_CalibTool = 0;
  }
  if (m_Akt4HI_EM_EtaJES_CalibTool)  {
    delete m_Akt4HI_EM_EtaJES_CalibTool;
    m_Akt4HI_EM_EtaJES_CalibTool = 0;
  }
  if (m_Akt4HI_Constit_EtaJES_CalibTool)  {
    delete m_Akt4HI_Constit_EtaJES_CalibTool;
    m_Akt4HI_Constit_EtaJES_CalibTool = 0;
  }
  if (m_isCollisions && m_Akt4HI_Insitu_CalibTool)  {
    delete m_Akt4HI_Insitu_CalibTool;
    m_Akt4HI_Insitu_CalibTool = 0;
  }

  // Delete egamma tools
  if (m_egammaPtEtaPhiECorrector)  {
    delete m_egammaPtEtaPhiECorrector;
    m_egammaPtEtaPhiECorrector = 0;
  }

  if (m_electronTightIsEMSelector)  {
    delete m_electronTightIsEMSelector;
    m_electronTightIsEMSelector = 0;
  }

  if (m_electronLooseIsEMSelector)  {
    delete m_electronLooseIsEMSelector;
    m_electronLooseIsEMSelector = 0;
  }

  if (!m_isCollisions && m_fudgeMCTool)  {
    delete m_fudgeMCTool;
    m_fudgeMCTool = 0;
  }

  if (m_photonTightIsEMSelector)  {
    delete m_photonTightIsEMSelector;
    m_photonTightIsEMSelector = 0;
  }

  // Delete muon tools
  if (m_muonPtEtaPhiECorrector)  {
    delete m_muonPtEtaPhiECorrector;
    m_muonPtEtaPhiECorrector = 0;
  }

  if (m_muonTightSelector)  {
    delete m_muonTightSelector;
    m_muonTightSelector = 0;
  }

  if (m_muonLooseSelector)  {
    delete m_muonLooseSelector;
    m_muonLooseSelector = 0;
  }

  //sleep (600);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}



/**
 * Checks the summary value for some value.
 */
uint8_t xAODAnalysis :: getSum( const xAOD::TrackParticle& trk, xAOD::SummaryType sumType ) {
  uint8_t sumVal=0;
  if (!trk.summaryValue(sumVal, sumType)) {
    Error ("getSum()", "Could not get summary type %i", sumType);
  }
  return sumVal;
}



/**
 * Checks if the given track passes the minbias working point cuts.
 */
bool xAODAnalysis :: passMinBias( const xAOD::TrackParticle& trk, const float vtx_z ) {//const xAOD::Vertex* vtx) {
  if (fabs(trk.eta()) > 2.5) return false;
  if (trk.pt() < 500.) return false;

  bool expectIBL = getSum(trk, xAOD::expectInnermostPixelLayerHit);
  bool expectBL  = getSum(trk, xAOD::expectNextToInnermostPixelLayerHit);
  uint8_t nIBL   = getSum(trk, xAOD::numberOfInnermostPixelLayerHits);
  uint8_t nBL    = getSum(trk, xAOD::numberOfNextToInnermostPixelLayerHits);

  if (expectIBL) {
    if (nIBL < 1) return false;
  } else {
    if (expectBL && nBL < 1) return false;
  }

  uint8_t nPixHits = getSum(trk, xAOD::numberOfPixelHits) + getSum(trk, xAOD::numberOfPixelDeadSensors);
  if (nPixHits < 1) return false;
  uint8_t nSctHits = getSum(trk, xAOD::numberOfSCTHits) + getSum(trk, xAOD::numberOfSCTDeadSensors);
  if (nSctHits < 6) return false;

  if (trk.pt() > 10.0*1e3 && TMath::Prob(trk.chiSquared(), trk.numberDoF()) < 0.01) return false;

  if (fabs(trk.d0()) > 1.5) return false;
  //if (vtx != nullptr) {
  if (fabs(trk.z0() + trk.vz() - vtx_z)*sin(trk.theta()) > 1.5) return false;
  //}
  return true;
}


/**
 * Returns the equivalent angle in the range 0 to 2pi.
 */
float xAODAnalysis :: InTwoPi (float phi) {
  while (phi < 0 || 2*3.14159 <= phi) {
   if (phi < 0) phi += 2*3.14159;
   else phi -= 2*3.14159;
  }
  return phi;
}


/**
 * Returns the difference between two angles in 0 to pi.
 * If sign is true, will return a signed version such that phi2 = phi1 + dphi
 */
float xAODAnalysis :: DeltaPhi (float phi1, float phi2) {
  phi1 = InTwoPi(phi1);
  phi2 = InTwoPi(phi2);
  float dphi = abs(phi1 - phi2);
  while (dphi > 3.14159) dphi = abs (dphi - 2*3.14159);

  return dphi;
}


/**
 * Returns dR between two eta, phi coordinates.
 */
float xAODAnalysis :: DeltaR (const float eta1, const float eta2, const float phi1, const float phi2) {
 const float deta = eta1 - eta2;
 const float dphi = DeltaPhi (phi1, phi2);
 return sqrt (pow (deta, 2) + pow (dphi, 2));
}
