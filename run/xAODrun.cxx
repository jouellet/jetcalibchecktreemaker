void xAODrun(const string outputStr, const string submitDir, const string sample_in, const bool isPeriodA, const bool isMC, const bool isOverlayMC, const bool getTracks) {

    // Set up the job for xAOD access:
    xAOD::Init().ignore();

//    string m_JES_config_file = "JES_MC15CHI_060316.config"; // pp, PbPb MC calibration
    std::string m_JES_config_file_hi_em;
    std::string m_JES_config_file_hi_constit;
    std::string m_JES_config_file_emtopo;
    // note the orientation of the constit scale config files: pPb = proton going to left --> applies to period A collisions (and vice versa)
    // for em scale calibrations, direction definition changed (stupidly)
    if (isPeriodA) {
      m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_pPb.config";
      m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_pPb.config";
      m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_pPb.config";
    }
    else {
      m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_Pbp.config";
      m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_Pbp.config";
      m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_Pbp.config";
    }

    // Construct the samples to run on: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SampleHandler
    // this object manage multiples samples in a directory
    SH::SampleHandler sh;  

    cout<<"outputStr: "<<outputStr<<endl;
    cout<<"submitDir: "<<submitDir<<endl;
    cout<<"sample_in: "<<sample_in<<endl;

    SH::scanRucio (sh, sample_in); 

    sh.setMetaString( "nc_grid_filter", "*" );
    sh.setMetaString( "nc_tree", "CollectionTree" );

    sh.print();

    // Set up the job for xAOD access:

    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler(sh);
    //job.options()->setDouble (EL::Job::optMaxEvents, 500);

    EL::OutputStream output ("myOutput");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
    job.algsAdd (ntuple);

    // Add our analysis to the job:
    cout << "isMC: " << isMC << endl;
    cout << "isOverlayMC: " << isOverlayMC << endl;

    xAODAnalysis* alg = new xAODAnalysis(m_JES_config_file_hi_em, m_JES_config_file_hi_constit, m_JES_config_file_emtopo, isMC, isOverlayMC, getTracks);
    job.algsAdd( alg );
    alg->outputName = "myOutput";

    // To automatically delete submitDir
    job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

    EL::PrunDriver driver;

    if (!outputStr.data()) Error("xAODrun", "Set outputStr!");

    // driver.options()->setDouble("nc_disableAutoRetry", 1); //Disabling AutoRetry

    driver.options()->setString(EL::Job::optGridNFilesPerJob, "1");
    //  driver.options()->setString("nc_rootVer", "5.34.22");
    //driver.options()->setString("nc_cmtConfig", "x86_64-slc6-gcc49-opt");
    driver.options()->setString("nc_outputSampleName", outputStr.c_str());

    driver.submitOnly( job, submitDir );

    return;
}
