#!/usr/bin/python

import os, sys

param=sys.argv

dir = "/afs/cern.ch/user/j/jeouelle/jetCalibCheckTreeMaker/run/output"
versionString = "400"
getTracks = False

#### #### #### #### #### Zee samples #### #### #### #### ####

ZeeSampleList = [
  "mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e6430_d1460_r10136_r9647", # pPb boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5363_s3164_r9436_r9006", # pPb boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5363_s3165_r9437_r9006", # pPb boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5363_s3166_r9438_r9006", # pPb boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5363_s3167_r9439_r9006", # pPb boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5363_s3168_r9440_r9006", # pPb boost
  "mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e6431_d1461_r10136_r9647" # Pbp boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5365_s3164_r9441_r9006", # Pbp boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5365_s3165_r9442_r9006", # Pbp boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5365_s3166_r9443_r9006", # Pbp boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5365_s3167_r9444_r9006", # Pbp boost
  #"mc15_pPb8TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e5365_s3168_r9445_r9006"  # Pbp boost
]

#PeriodList = [False, False, False, False, False, True, True, True, True, True]
PeriodList = [True, False]

for i,sample in enumerate(ZeeSampleList):
  periodA = PeriodList[i]

  command = "root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "
  output = "user.jeouelle.2.4.30hi.calibcheck."
  output += versionString
  output += ".mc15_8TeV." + sample[13:19] + ".ZeeJet"
  #output += str((i%5)+1) # for signal-only samples

  if periodA:
    output += ".pPb"
  else:
    output += ".Pbp"

  submitDir = dir + "/Zee" 
  #submitDir = dir + "/Zee" + str((i%5) + 1) # for signal-only samples
  if periodA:
    submitDir += "_pPb"
  else:
    submitDir += "_Pbp"
  
  commandOut = " > logfiles/logfile_"
  commandOut += "Zee"
  #commandOut += "Zee" + str((i%5) + 1) # for signal-only samples
  if periodA:
    commandOut += "_pPb"
  else:
    commandOut += "_Pbp"

  commandOut += " 2> errors/errors_"
  commandOut += "Zee" + str((i%5) + 1)
  if periodA:
    commandOut += "_pPb"
  else:
    commandOut += "_Pbp"

  fullCommand = command + "'xAODrun.cxx (\"" + output + "\", \"" + submitDir + "\", \"" + sample + "\", " + str(int(periodA)) + ", 1, 1, " + str(int(getTracks)) + ")'" + commandOut + " &"
  print fullCommand
  os.system(fullCommand)

