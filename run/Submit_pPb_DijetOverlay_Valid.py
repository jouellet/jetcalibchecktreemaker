#!/usr/bin/python

import os, sys

param=sys.argv

dir = "/afs/cern.ch/user/j/jeouelle/jetCalibCheckTreeMaker/run/output"
versionString = "430"
getTracks = False

list=[
    "JZ2"
]

for periodA in [True, False]:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ2" and not periodA:
      sample = "mc15_valid.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.merge.AOD.e6395_d1490_r10779_r9647" # Pbp boost

    elif JZ == "JZ2" and periodA:
      sample = "mc15_valid.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.merge.AOD.e6394_d1489_r10779_r9647"  # pPb boost

    command = "root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "

    output = "user.jeouelle.2.4.30hi.calibcheck."
    output += versionString
    output += ".mc15_8TeV." + sample[11:17] + ".jetjet." + JZ + "R04"
    if periodA:
      output += ".pPb"
    else:
      output += ".Pbp"

    submitDir = dir + "/" + JZ + "_8TeV" 
    if periodA:
      submitDir += "_pPb"
    else:
      submitDir += "_Pbp"
    
    commandOut = " > logfiles/logfile_"
    commandOut += JZ
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"

    commandOut += " 2> errors/errors_"
    commandOut += JZ
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"
  
    fullCommand = command + "'xAODrun.cxx (\"" + output + "\", \"" + submitDir + "\", \"" + sample + "\", " + str(int(periodA)) + ", 1, 1, " + str(int(getTracks)) + ")'" + commandOut + " &"
    print fullCommand
    os.system(fullCommand)

