#!/usr/bin/python

import os, sys

param=sys.argv

dir = "/afs/cern.ch/user/j/jeouelle/jetCalibCheckTreeMaker/run/output"
versionString = "401"
getTracks = False

list=[
    "Slice1",
    "Slice2",
    "Slice3",
    "Slice4",
    "Slice5",
    "Slice6"
]

for periodA in [True]:
  for Slice in list:
    sample = ""
    # Setting the correct sample
    if Slice == "Slice1" and periodA:
      sample = "mc15_valid.423100.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP17_35.merge.AOD.e5709_s3084_r9160_r9006" #signal only sample

    elif Slice == "Slice2" and periodA:
      sample = "mc15_valid.423101.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP35_50.merge.AOD.e5709_s3084_r9160_r9006" #signal only sample

    elif Slice == "Slice3" and periodA:
      sample = "mc15_valid.423102.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP50_70.merge.AOD.e5709_s3084_r9160_r9006" #signal only sample

    elif Slice == "Slice4" and periodA:
      sample = "mc15_valid.423103.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP70_140.merge.AOD.e5709_s3084_r9160_r9006" #signal only sample

    elif Slice == "Slice5" and periodA:
      sample = "mc15_valid.423104.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP140_280.merge.AOD.e5709_s3084_r9160_r9006" #signal only sample

    elif Slice == "Slice6" and periodA:
      sample = "mc15_valid.423105.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP280_500.merge.AOD.e5709_s3084_r9160_r9006" #signal only sample

    command = "root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "

    output = "user.jeouelle.2.4.30hi.calibcheck."

    output += versionString
    output += ".mc15_8TeV." + sample[11:17] + ".signalonly" # gets the production number (validation samples only)
    output += "." + Slice
    if periodA:
      output += ".pPb"
    else:
      output += ".Pbp"

    submitDir = dir + "/" + str(Slice)
    if periodA:
      submitDir += "_pPb"
    else:
      submitDir += "_Pbp"
    
    commandOut = " > logfiles/logfile_"
    commandOut += str(Slice)
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"

    commandOut += " 2> errors/errors_"
    commandOut += str(Slice)
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"

    fullCommand = command + "'xAODrun.cxx (\"" + output + "\", \"" + submitDir + "\", \"" + sample + "\", " + str(int(periodA)) + ", 1, 0, " + str(int(getTracks)) + ")'" + commandOut + " &"
    print fullCommand
    os.system(fullCommand)
    ## end loop over list ##

