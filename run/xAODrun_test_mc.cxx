void xAODrun_test_mc () {

    //
    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    string m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_Pbp.config"; // use Pbp for period B
    string m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_Pbp.config"; // new configs have opposite orientation, but only in MC
    string m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_Pbp.config";

    string input = "/afs/cern.ch/work/j/jeouelle/mc15_pPb8TeV"; // default for local run
    string submitDir = "submitDir";

    // Construct the samples to run on: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SampleHandler
    // this object manage multiples samples in a directory
    SH::SampleHandler sh;  

    cout<<"input: "<<input<<endl;

    const char* inputFilePath = gSystem->ExpandPathName(input.c_str());
//    SH::ScanDir().filePattern("AOD.14612251.*").scan(sh, inputFilePath); dijet overlay validation sample
//    SH::ScanDir().filePattern("AOD.11670199.*").scan(sh, inputFilePath); // gammajet DP140_280 period B overlay sample
    SH::ScanDir().filePattern("AOD.13224270.*").scan(sh, inputFilePath); // Zee period A overlay sample
    bool isMC = true;
    bool isOverlayMC = true;

    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );


    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler(sh);
    //job.options()->setDouble (EL::Job::optMaxEvents, 20);

    EL::OutputStream output  ("myOutput");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
    job.algsAdd (ntuple);

    // Add our analysis to the job:
    xAODAnalysis* alg = new xAODAnalysis(m_JES_config_file_hi_em, m_JES_config_file_hi_constit, m_JES_config_file_emtopo, isMC, isOverlayMC, false);
    job.algsAdd( alg );

    alg->outputName = "myOutput"; // give the name of the output to our algorithm

    // To automatically delete submitDir
    job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);


    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
    driver.options()->setString("nc_outputSampleName", submitDir);

    driver.submit( job, submitDir );

    return;
}
