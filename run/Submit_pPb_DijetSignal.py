#!/usr/bin/python

import os, sys

param=sys.argv

dir = "/afs/cern.ch/user/j/jeouelle/jetCalibCheckTreeMaker/run/output"
versionString = "400"
getTracks = False

#list=[
#    "JZ1",
#    "JZ2",
#    "JZ3",
#    "JZ4",
#    "JZ5"
#]
#
#for periodA in [True, False]:


list=[
#    "JZ1",
    "JZ2",
#    "JZ3",
#    "JZ4",
#    "JZ5"
]

for periodA in [True, False]:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1" and not periodA:
      sample = "mc15_pPb8TeV.420011.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1R04.merge.AOD.e6519_s3084_s3153_r9985_r9647_tid13211339_00" # Pbp boost

    elif JZ == "JZ1" and periodA:
      sample = "mc15_pPb8TeV.420011.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1R04.merge.AOD.e6518_s3084_s3153_r9985_r9647_tid13216170_00" # pPb boost

    elif JZ == "JZ2" and not periodA:
      #sample = "mc15_pPb8TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.merge.AOD.e6395_s3084_s3153_r9985_r9647_tid13202455_00" # Pbp boost
      sample = "mc15_valid.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.recon.AOD.e6395_s3084_s3153_r10826" # validation sample with 100ns bunch spacing instead of 25ns

    elif JZ == "JZ2" and periodA:
      #sample = "mc15_pPb8TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.merge.AOD.e6394_s3084_s3153_r9985_r9647_tid13200714_00" # pPb boost
      sample = "mc15_valid.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.recon.AOD.e6394_s3084_s3153_r10826" # validation sample with 100ns bunch spacing instead of 25ns

    elif JZ == "JZ3" and not periodA:
      sample = "mc15_pPb8TeV.420013.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3R04.merge.AOD.e6519_s3084_s3153_r9985_r9647_tid13211348_00" # Pbp boost

    elif JZ == "JZ3" and periodA:
      sample = "mc15_pPb8TeV.420013.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3R04.merge.AOD.e6518_s3084_s3153_r9985_r9647_tid13211323_00" # pPb boost

    elif JZ == "JZ4" and not periodA:
      sample = "mc15_pPb8TeV.420014.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4R04.merge.AOD.e6519_s3084_s3153_r9985_r9647_tid13214408_00" # Pbp boost

    elif JZ == "JZ4" and periodA:
      sample = "mc15_pPb8TeV.420014.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4R04.merge.AOD.e6518_s3084_s3153_r9985_r9647_tid13237282_00" # pPb boost

    elif JZ == "JZ5" and not periodA:
      sample = "mc15_pPb8TeV.420015.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5R04.merge.AOD.e6519_s3084_s3153_r9985_r9647_tid13214423_00" # Pbp boost

    elif JZ == "JZ5" and periodA:
      sample = "mc15_pPb8TeV.420015.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5R04.merge.AOD.e6518_s3084_s3153_r9985_r9647_tid13211331_00" # pPb boost

    command = "root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "

    output = "user.jeouelle.2.4.30hi.calibcheck."
    output += versionString
    output += ".mc15_8TeV." + sample[13:19] + ".jetjet." + JZ + "R04"
    if periodA:
      output += ".pPb"
    else:
      output += ".Pbp"

    submitDir = dir + "/" + JZ + "_8TeV" 
    if periodA:
      submitDir += "_pPb"
    else:
      submitDir += "_Pbp"
    
    commandOut = " > logfiles/logfile_"
    commandOut += JZ
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"

    commandOut += " 2> errors/errors_"
    commandOut += JZ
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"
  
    fullCommand = command + "'xAODrun.cxx (\"" + output + "\", \"" + submitDir + "\", \"" + sample + "\", " + str(int(periodA)) + ", 1, 1, " + str(int(getTracks)) + ")'" + commandOut + " &"
    print fullCommand
    os.system(fullCommand)

