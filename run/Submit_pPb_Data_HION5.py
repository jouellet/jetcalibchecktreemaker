#!/usr/bin/python

import os, sys

param=sys.argv

dir = "/afs/cern.ch/user/j/jeouelle/jetCalibCheckTreeMaker/run/output"
versionString = "370"
getTracks = False

list=[
#  "data16_hip8TeV.00313063.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313067.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313100.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313107.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313136.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313187.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313259.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
  "data16_hip8TeV.00313285.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313295.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313333.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313435.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313572.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313574.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313575.physics_Main.merge.DAOD_HION5.f774_m1736_p2961",
#  "data16_hip8TeV.00313603.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313629.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313630.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313688.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313695.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313833.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313878.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313929.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313935.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00313984.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00314014.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00314077.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00314105.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00314112.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00314157.physics_Main.merge.DAOD_HION5.f781_m1741_p2961",
#  "data16_hip8TeV.00314170.physics_Main.merge.DAOD_HION5.f781_m1741_p2961"
]

for i,sample in enumerate(list):
    runNumber = int(sample[17:23])
    periodA = runNumber < 313500
      
    command = "root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "

    output = "user.jeouelle.2.4.30hi.calibcheck.HION5."
    output += versionString + "."
    if periodA:
      output += "pPb.R04."
    else:
      output += "Pbp.R04."
    output += "8TeV." + sample[15:]

    submitDir = dir + "/" + "run_" + str(runNumber)
    if periodA:
      submitDir += "_pPb"
    else:
      submitDir += "_Pbp"
    
    commandOut = " > logfiles/logfile_"
    commandOut += str(runNumber)
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"

    commandOut += " 2> errors/errors_"
    commandOut += str(runNumber)
    if periodA:
      commandOut += "_pPb"
    else:
      commandOut += "_Pbp"

    fullCommand = command + "'xAODrun.cxx (\"" + output + "\", \"" + submitDir + "\", \"" + sample + "\", " + str(int(periodA)) + ", 0, 0, " + str(int(getTracks)) + ")'" + commandOut
    if i % 10 != 9:
      fullCommand = fullCommand + " &"
    print fullCommand
    os.system(fullCommand)

