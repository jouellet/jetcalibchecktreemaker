#!/usr/bin/python

import os, sys

param=sys.argv

dir = "/afs/cern.ch/user/j/jeouelle/jetCalibCheckTreeMaker/run/output"
versionString = "400"
getTracks = False

#### #### #### #### #### Zmumu samples #### #### #### #### ####

ZmumuSampleList = [
  "mc15_pPb8TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e6431_d1461_r10136_r9647", # Pbp boost
  #"mc15_pPb8TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e6430_d1460_r10136_r9647"  # pPb boost
]

PeriodList = [False]#, True]

for i,sample in enumerate(ZmumuSampleList): 
  periodA = PeriodList[i]

  command = "root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "
  output = "user.jeouelle.2.4.30hi.calibcheck."
  output += versionString
  output += ".mc15_8TeV." + sample[13:19] + ".ZmumuJet"

  if periodA:
    output += ".pPb"
  else:
    output += ".Pbp"

  submitDir = dir + "/Zmumu"
  if periodA:
    submitDir += "_pPb"
  else:
    submitDir += "_Pbp"
  
  commandOut = " > logfiles/logfile_"
  commandOut += "Zmumu"
  if periodA:
    commandOut += "_pPb"
  else:
    commandOut += "_Pbp"

  commandOut += " 2> errors/errors_"
  commandOut += "Zmumu"
  if periodA:
    commandOut += "_pPb"
  else:
    commandOut += "_Pbp"

  fullCommand = command + "'xAODrun.cxx (\"" + output + "\", \"" + submitDir + "\", \"" + sample + "\", " + str(int(periodA)) + ", 1, 1, " + str(int(getTracks)) + ")'" + commandOut + " &"
  print fullCommand
  os.system(fullCommand)

  ## end loop over periods ##
