void xAODrun_test () {

    //
    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SampleHandler
    // this object manage multiples samples in a directory
    SH::SampleHandler sh;  

    // for testing xAOD
    // Period A
    const std::string input = "/afs/cern.ch/work/j/jeouelle/public/data16_hip8TeV"; // default for local run
    const std::string filePattern = "data16_hip8TeV.00313063.physics_Main.recon.AOD.f774_m1736.*";
    const std::string m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_pPb.config";
    const std::string m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_pPb.config";
    const std::string m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_pPb.config";

    // Period B
    //const std::string input = "/afs/cern.ch/work/j/jeouelle/data16_hip8TeV"; // default for local run
    //const std::string filePattern = "data16_hip8TeV.00313630.physics_Main.recon.AOD.f781_m1741.*";
    //const std::string m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_Pbp.config";
    //const std::string m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_Pbp.config";
    //const std::string m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_Pbp.config";

    //// for testing HION5 derivation
    //const std::string input = "/afs/cern.ch/work/j/jeouelle/data16_hip8TeV_hion5";
    //const std::string filePattern = "DAOD_HION5.10434418._000053.pool.root.1";
    //const std::string m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_pPb.config";
    //const std::string m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_pPb.config";
    //const std::string m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_pPb.config";

    //// for dennis
    //const std::string input = "/afs/cern.ch/user/d/dvp/public/";
    //const std::string filePattern = "*AOD*";
    //const std::string m_JES_config_file_hi_constit = "JES_MC15c_HI_Apr2018_pPb.config";
    //const std::string m_JES_config_file_hi_em = "JES_MC15c_HI_Oct2018_pPb.config";
    //const std::string m_JES_config_file_emtopo = "JES_MC15c_HI_Oct2018_pPb.config";

    const std::string submitDir = "submitDir";

    std::cout << "input: " << input << std::endl;

    const char* inputFilePath = gSystem->ExpandPathName(input.c_str());
//    SH::ScanDir().filePattern("data16_hip8TeV.00313063.physics_Main.recon.AOD.f774_m1736.*").scan(sh, inputFilePath); // period A test
    SH::ScanDir().filePattern(filePattern.c_str()).scan(sh, inputFilePath); // period B test

    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );


    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler(sh);
    //job.options()->setDouble (EL::Job::optMaxEvents, 1000);

    EL::OutputStream output  ("myOutput");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
    job.algsAdd (ntuple);

    // Add our analysis to the job:
    xAODAnalysis* alg = new xAODAnalysis(m_JES_config_file_hi_em, m_JES_config_file_hi_constit, m_JES_config_file_emtopo, false, false, false);
    job.algsAdd( alg );

    alg->outputName = "myOutput"; // give the name of the output to our algorithm

    // To automatically delete submitDir
    job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);


    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
    driver.options()->setString("nc_outputSampleName", submitDir);

    driver.submit( job, submitDir );

    return;
}
